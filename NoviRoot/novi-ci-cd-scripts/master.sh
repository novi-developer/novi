#!/bin/bash

branch_name=$1
folder_dir=$2

# Ensure script exit when any kind of error occur
set -e

echo "===============RUNNING TEST STAGE================="
chmod +x 0-test-and-report.sh
./0-test-and-report.sh $branch_name $folder_dir

echo "===============RUNNING DEPLOY STAGE==============="
chmod +x 1-deploy.sh
./1-deploy.sh $branch_name $folder_dir

echo "==================ALL JOBS DONE!=================="
