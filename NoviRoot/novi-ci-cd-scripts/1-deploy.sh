#!/bin/bash

branch_name=$1
folder_dir=$2

# Ensure script exit when any kind of error occur
set -e

cd $folder_dir

echo "==========Creating netrc auth for Heroku=========="
echo "machine api.heroku.com" > ~/.netrc
echo "    login $HEROKU_EMAIL" >> ~/.netrc
echo "    password $HEROKU_API_KEY" >> ~/.netrc
echo "machine git.heroku.com" >> ~/.netrc
echo "    login $HEROKU_EMAIL" >> ~/.netrc
echo "    password $HEROKU_API_KEY" >> ~/.netrc

if [[ $branch_name == "master" ]]; then
    echo "=================Deploying MASTER================="

    echo "===========Deploying Eureka Production============"
    heroku git:remote -a novi-eureka
    git push heroku `git subtree push --prefix=EurekaRegistryService master`:master --force

    echo "=========Deploying Bot Service Production========="
    heroku git:remote -a novi-bot
    git push heroku `git subtree split --prefix=BotService master`:master --force

    echo "=============Deploying MS Production=============="
    heroku git:remote -a novi-ms
    git subtree push --prefix=MovieService heroku master

    echo "=============Deploying RRS Production============="
    heroku git:remote -a novi-rrs
    git subtree push --prefix=RecommendationAndReviewService heroku master

    echo "=============Deploying WBS Production============="
    heroku git:remote -a novi-wbs
    git subtree push --prefix=WatchAndBuyService heroku master

    echo "=============Deploying STS Production============="
    heroku git:remote -a novi-sts
    git push heroku `git subtree push --prefix=ShowtimesService master`:master --force

    echo "=============Deploying WLS Production============="
    heroku git:remote -a novi-wls
    git push heroku `git subtree push --prefix=WatchlistService master`:master --force

elif [[ $branch_name == "staging-movieservice" ]]; then
    echo "===============Deploying STAGING MS==============="

    echo "=============Deploying Eureka Staging============="
    heroku git:remote -a novi-eureka-staging
    git push heroku `git subtree push --prefix=EurekaRegistryService master`:master --force

    echo "==========Deploying Bot Service Staging==========="
    heroku git:remote -a novi-bot-staging-ms
    git push heroku `git subtree split --prefix=BotService master`:master --force

    echo "===============Deploying MS Staging==============="
    heroku git:remote -a novi-ms-staging
    git subtree push --prefix=MovieService heroku master

elif [[ $branch_name == "staging-recommendationandreviewservice" ]]; then
    echo "===============Deploying STAGING RRS=============="

    echo "=============Deploying Eureka Staging============="
    heroku git:remote -a novi-eureka-staging
    git push heroku `git subtree push --prefix=EurekaRegistryService master`:master --force

    echo "===========Deploying Bot Service Staging=========="
    heroku git:remote -a novi-bot-staging-rrs
    git push heroku `git subtree split --prefix=BotService master`:master --force

    echo "===============Deploying RRS Staging=============="
    heroku git:remote -a novi-rrs-staging
    git subtree push --prefix=RecommendationAndReviewService heroku master

elif [[ $branch_name == "staging-watchandbuyservice" ]]; then
    echo "===============Deploying STAGING WBS=============="

    echo "=============Deploying Eureka Staging============="
    heroku git:remote -a novi-eureka-staging
    git push heroku `git subtree push --prefix=EurekaRegistryService master`:master --force

    echo "===========Deploying Bot Service Staging=========="
    heroku git:remote -a novi-bot-staging-wbs
    git push heroku `git subtree split --prefix=BotService master`:master --force

    echo "===============Deploying WBS Staging=============="
    heroku git:remote -a novi-wbs-staging
    git subtree push --prefix=WatchAndBuyService heroku master

elif [[ $branch_name == "staging-showtimesservice" ]]; then
    echo "===============Deploying STAGING STS=============="

    echo "=============Deploying Eureka Staging============="
    heroku git:remote -a novi-eureka-staging
    git push heroku `git subtree push --prefix=EurekaRegistryService master`:master --force

    echo "===========Deploying Bot Service Staging=========="
    heroku git:remote -a novi-bot-staging-sts
    git push heroku `git subtree split --prefix=BotService master`:master --force

    echo "===============Deploying STS Staging=============="
    heroku git:remote -a novi-sts-staging
    git push heroku `git subtree push --prefix=ShowtimesService master`:master --force

elif [[ $branch_name == "staging-watchlistservice" ]]; then
    echo "===============Deploying STAGING WLS=============="

    echo "=============Deploying Eureka Staging============="
    heroku git:remote -a novi-eureka-staging
    git push heroku `git subtree push --prefix=EurekaRegistryService master`:master --force

    echo "===========Deploying Bot Service Staging=========="
    heroku git:remote -a novi-bot-staging-wls
    git push heroku `git subtree split --prefix=BotService master`:master --force

    echo "===============Deploying WLS Staging=============="
    heroku git:remote -a novi-wls-staging
    git push heroku `git subtree push --prefix=WatchlistService master`:master --force
fi

echo "=================Deployment Done!================="