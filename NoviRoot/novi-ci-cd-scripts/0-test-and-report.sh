#!/bin/bash

branch_name=$1
folder_dir=$2

# Ensure script exit when any kind of error occur
set -e

echo "=====Ensuring gradle wrapper can be executed======"
cd $folder_dir
chmod +x gradlew
echo "===============Testing Bot Service================"
./gradlew :BotService:test
echo "=========Testing Eureka Registry Service=========="
./gradlew :EurekaRegistryService:test

if [[ ${branch_name} == "master" || ${branch_name} == "staging-movieservice" ]]
then
    echo "==============Testing Movie Service==============="
    ./gradlew :MovieService:test
fi

if [[ ${branch_name} == "master" || ${branch_name} == "staging-recommendationandreviewservice" ]]
then
    echo "=====Testing Recommendation and Review Service===="
    ./gradlew :RecommendationAndReviewService:test
fi

if [[ ${branch_name} == "master" || ${branch_name} == "staging-watchandbuyservice" ]]
then
    echo "==========Testing Watch and Buy Service==========="
    ./gradlew :WatchAndBuyService:test
fi

if [[ ${branch_name} == "master" || ${branch_name} == "staging-showtimesservice" ]]
then
    echo "============Testing Showtimes Service============="
    ./gradlew :ShowtimesService:test
fi

if [[ ${branch_name} == "master" || ${branch_name} == "staging-watchlistservice" ]]
then
    echo "============Testing Watch List Service============"
    ./gradlew :WatchlistService:test
fi

echo "============Generate Jacoco Test Report==========="
./gradlew jacocoTestReport

coverage=$(cat NoviRoot/reports/jacocoTestReportHtml/index.html | grep -Po "Total.*?\K([0-9]{1,3})%")
echo "============Total Coverage: $coverage==========="

echo "==================Testing done!==================="