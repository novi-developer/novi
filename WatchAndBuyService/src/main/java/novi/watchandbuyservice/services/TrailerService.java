package novi.watchandbuyservice.services;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import novi.watchandbuyservice.models.TrailerLinkData;
import novi.watchandbuyservice.suppliers.TrailerLinkBubbleSupplier;
import novi.watchandbuyservice.suppliers.TrailerLinkDataSupplier;
import novi.watchandbuyservice.suppliers.TrailerLinkFlexMessageSupplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrailerService {
    @Autowired
    private RestTemplate restClient;

    @Value("${youtube.api.key}")
    private String youtubeAPIKey;

    private String youtubeQueryFormat = "https://www.googleapis.com/youtube/v3/search" +
                                        "?part=snippet&key=%s" +
                                        "&maxResults=3&order=relevance" +
                                        "&q=%s&type=video";

    public Message handle(String argument) {
        String filmName = argument.concat(" official trailer");
        String youtubeQuery = String.format(youtubeQueryFormat, youtubeAPIKey, filmName);

        String youtubeQueryResult = restClient.getForObject(youtubeQuery, String.class);
        TrailerLinkDataSupplier trailerLinkDataSupplier = new TrailerLinkDataSupplier(youtubeQueryResult);
        List<TrailerLinkData> trailerLinkDataList = trailerLinkDataSupplier.get();
        if (!trailerLinkDataList.isEmpty()) {
            List<Bubble> trailerLinkBubbleList = new ArrayList<>();
            for (TrailerLinkData trailerLinkData : trailerLinkDataList) {
                TrailerLinkBubbleSupplier trailerLinkBubbleSupplier = new TrailerLinkBubbleSupplier(trailerLinkData);
                trailerLinkBubbleList.add(trailerLinkBubbleSupplier.get());
            }
            TrailerLinkFlexMessageSupplier trailerLinkFlexMessageSupplier = new TrailerLinkFlexMessageSupplier(trailerLinkBubbleList);
            FlexMessage trailerFlexMessage = trailerLinkFlexMessageSupplier.get();
            return trailerFlexMessage;
        } else {
            String failMessage = "Maaf, pencarian trailer gagal dilakukan.";
            return new TextMessage(failMessage);
        }
    }
}
