package novi.watchandbuyservice.services;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import novi.watchandbuyservice.models.BuyLinkData;
import novi.watchandbuyservice.suppliers.BuyLinkDataBubbleSupplier;
import novi.watchandbuyservice.suppliers.BuyLinkDataSupplier;
import novi.watchandbuyservice.suppliers.BuyLinkFlexMessageSupplier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BuyService {
    private String gPlayURLFormat = "https://play.google.com/store/search?q=%s&c=movies&hl=en";

    public Message handle(String argument) {
        String filmNameQuery = argument.replace(" ", "+");
        String gPlayQueryURL = String.format(gPlayURLFormat, filmNameQuery);

        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);

        try {
            HtmlPage gPlayResultPage = client.getPage(gPlayQueryURL);

            BuyLinkDataSupplier buyLinkDataSupplier = new BuyLinkDataSupplier(gPlayResultPage);
            List<BuyLinkData> buyLinkDataList = buyLinkDataSupplier.get();

            if (!buyLinkDataList.isEmpty()) {
                List<Bubble> buyLinkBubbleList = new ArrayList<>();
                for (BuyLinkData buyLinkData : buyLinkDataList) {
                    BuyLinkDataBubbleSupplier buyLinkDataBubbleSupplier = new BuyLinkDataBubbleSupplier(buyLinkData);
                    buyLinkBubbleList.add(buyLinkDataBubbleSupplier.get());
                }

                BuyLinkFlexMessageSupplier buyLinkFlexMessageSupplier = new BuyLinkFlexMessageSupplier(buyLinkBubbleList);
                FlexMessage buyLinkFlexMessage = buyLinkFlexMessageSupplier.get();
                return buyLinkFlexMessage;
            } else {
                return new TextMessage("Pencarian link pembelian di Google Play gagal.");
            }
        } catch (IOException ioException) {
            return new TextMessage("Pencarian link pembelian di Google Play gagal.");
        } finally {
            client.close();
        }
    }
}