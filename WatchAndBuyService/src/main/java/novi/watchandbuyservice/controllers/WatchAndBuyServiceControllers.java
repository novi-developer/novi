package novi.watchandbuyservice.controllers;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.watchandbuyservice.clients.BotServiceClient;
import novi.watchandbuyservice.models.NoviReplyMessage;
import novi.watchandbuyservice.services.BuyService;
import novi.watchandbuyservice.services.TrailerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableFeignClients
public class WatchAndBuyServiceControllers {
    @Autowired
    private TrailerService trailerService;

    @Autowired
    private BotServiceClient botServiceClient;

    @Autowired
    private BuyService buyService;

    @GetMapping("/query")
    @ResponseStatus(value = HttpStatus.OK)
    public void handleQuery(@RequestParam("query") String query, @RequestParam("argument") String argument,
            @RequestParam("replyToken") String replyToken) {
        if (query.contains("/trailer")) {
            Message message = trailerService.handle(argument);
            this.sendMessage(replyToken, message);
        } else if (query.contains("/buy")) {
            Message message = buyService.handle(argument);
            this.sendMessage(replyToken, message);
        } else {
            TextMessage failureMessage = new TextMessage("Query tidak dapat diproses!");
            this.sendMessage(replyToken, failureMessage);
        }
    }

    @Async
    public void sendMessage(String replyToken, Message message) {
        NoviReplyMessage replyMessage = new NoviReplyMessage(replyToken, message);
        botServiceClient.send(replyMessage);
    }
}
