package novi.watchandbuyservice.suppliers;

import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import novi.watchandbuyservice.models.BuyLinkData;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class BuyLinkDataSupplier implements Supplier<List<BuyLinkData>> {
    private static String G_PLAY_BASE_URL= "https://play.google.com";
    private static String G_PLAY_SINGLE_RESULT = "//div[contains(@class, 'ZMQXF')]/a";
    private static String G_PLAY_SINGLE_RESULT_IMAGE = "//div[contains(@class, 'BKXI5 e8g5f')]/a/img";
    private static String G_PLAY_MULTIPLE_RESULT = "//div[contains(@class, 'b8cIId ReQCgd Q9MA7b')]/a";
    private static String G_PLAY_MULTIPLE_RESULT_IMAGE = "//span[contains(@class, 'yNWQ8e K3IMke')]/img";

    private HtmlPage gPlayResultPage;

    public BuyLinkDataSupplier(HtmlPage gPlayResultPage) {
        this.gPlayResultPage = gPlayResultPage;
    }

    @Override
    public List<BuyLinkData> get() {
        List<BuyLinkData> result = new ArrayList<>();

        List<HtmlElement> singleResult = this.gPlayResultPage.getByXPath(G_PLAY_SINGLE_RESULT);
        List<HtmlElement> singleResultImage = this.gPlayResultPage.getByXPath(G_PLAY_SINGLE_RESULT_IMAGE);
        if (singleResult.size() != 0) {
            HtmlElement movie = singleResult.get(0);
            HtmlElement movieImage = singleResultImage.get(0);

            String title = movie.getAttribute("title");
            String URL = G_PLAY_BASE_URL + movie.getAttribute("href");
            String imageURL = movieImage.getAttribute("src");

            result.add(new BuyLinkData(title, URL, imageURL));
        } else {
            List<HtmlElement> multipleResult = gPlayResultPage.getByXPath(G_PLAY_MULTIPLE_RESULT);
            List<HtmlElement> multipleResultImage = gPlayResultPage.getByXPath(G_PLAY_MULTIPLE_RESULT_IMAGE);
            for (int i = 0; i < Math.min(3, multipleResult.size()); i++) {
                HtmlElement movie = multipleResult.get(i);
                HtmlElement movieImage = multipleResultImage.get(i);

                String title = movie.asText();
                String URL= G_PLAY_BASE_URL + movie.getAttribute("href");
                String imageURL = movieImage.getAttribute("data-src");

                result.add(new BuyLinkData(title, URL, imageURL));
            }
        }

        return result;
    }
}
