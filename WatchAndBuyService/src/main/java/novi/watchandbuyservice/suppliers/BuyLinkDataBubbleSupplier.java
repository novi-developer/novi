package novi.watchandbuyservice.suppliers;

import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import novi.watchandbuyservice.models.BuyLinkData;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class BuyLinkDataBubbleSupplier implements Supplier<Bubble> {
    private static String LINK_BUTTON_TEXT = "Ketuk untuk membeli film";

    private BuyLinkData buyLinkData;

    public BuyLinkDataBubbleSupplier(BuyLinkData buyLinkData) {
        this.buyLinkData = buyLinkData;
    }

    @Override
    public Bubble get() {
        Image heroImage = Image.builder()
                .url(this.buyLinkData.getBuyImageURL())
                .size(Image.ImageSize.FULL_WIDTH)
                .aspectRatio(Image.ImageAspectRatio.R3TO4)
                .aspectMode(Image.ImageAspectMode.Cover)
                .build();

        Text titleText = Text.builder()
                .text(this.buyLinkData.getBuyTitle())
                .weight(Text.TextWeight.BOLD)
                .size(FlexFontSize.SM)
                .build();

        Box bodyBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(titleText))
                .build();

        Button linkButton = Button.builder()
                .style(Button.ButtonStyle.LINK)
                .height(Button.ButtonHeight.MEDIUM)
                .action(new URIAction(LINK_BUTTON_TEXT, this.buyLinkData.getBuyURL()))
                .build();

        Box footerBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(linkButton))
                .build();

        Bubble buyLinkBubble = Bubble.builder()
                .hero(heroImage)
                .body(bodyBox)
                .footer(footerBox)
                .build();

        return buyLinkBubble;
    }
}
