package novi.watchandbuyservice.suppliers;

import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import novi.watchandbuyservice.models.TrailerLinkData;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class TrailerLinkBubbleSupplier implements Supplier<Bubble> {
    private static String LINK_BUTTON_TEXT = "Ketuk untuk menonton Trailer";

    private TrailerLinkData trailerLinkData;

    public TrailerLinkBubbleSupplier(TrailerLinkData trailerLinkData) {
        this.trailerLinkData = trailerLinkData;
    }

    @Override
    public Bubble get() {
        Image heroImage = Image.builder()
                .url(this.trailerLinkData.getVideoThumbnailURL())
                .size(Image.ImageSize.FULL_WIDTH)
                .aspectRatio(Image.ImageAspectRatio.R16TO9)
                .aspectMode(Image.ImageAspectMode.Cover)
                .build();

        Text titleText = Text.builder()
                .text(this.trailerLinkData.getVideoTitle())
                .weight(Text.TextWeight.BOLD)
                .size(FlexFontSize.SM)
                .build();

        Box bodyBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(titleText))
                .build();

        Button linkButton = Button.builder()
                .style(Button.ButtonStyle.LINK)
                .height(Button.ButtonHeight.MEDIUM)
                .action(new URIAction(LINK_BUTTON_TEXT, this.trailerLinkData.getVideoURL()))
                .build();

        Box footerBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(linkButton))
                .build();

        Bubble trailerLinkBubble = Bubble.builder()
                .hero(heroImage)
                .body(bodyBox)
                .footer(footerBox)
                .build();

        return trailerLinkBubble;
    }
}
