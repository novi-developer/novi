package novi.watchandbuyservice.suppliers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.container.FlexContainer;

import java.util.List;
import java.util.function.Supplier;

public class BuyLinkFlexMessageSupplier implements Supplier<FlexMessage> {
    private static String DEFAULT_ALT_TEXT = "Buy Links";

    private List<Bubble> buyLinkBubbleList;

    public BuyLinkFlexMessageSupplier(List<Bubble> buyLinkBubbleList) {
        this.buyLinkBubbleList = buyLinkBubbleList;
    }

    @Override
    public FlexMessage get() {
        FlexContainer container = Carousel.builder()
                .contents(buyLinkBubbleList)
                .build();

        FlexMessage buyLinkFlexMessage = FlexMessage.builder()
                .altText(DEFAULT_ALT_TEXT)
                .contents(container)
                .build();

        return buyLinkFlexMessage ;
    }
}
