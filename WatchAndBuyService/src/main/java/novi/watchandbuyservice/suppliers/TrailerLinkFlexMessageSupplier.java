package novi.watchandbuyservice.suppliers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.container.FlexContainer;

import java.util.List;
import java.util.function.Supplier;

public class TrailerLinkFlexMessageSupplier implements Supplier<FlexMessage> {
    private static String DEFAULT_ALT_TEXT = "Trailer Links";

    private List<Bubble> trailerLinkBubbleList;

    public TrailerLinkFlexMessageSupplier(List<Bubble> trailerLinkBubbleList) {
        this.trailerLinkBubbleList = trailerLinkBubbleList;
    }

    @Override
    public FlexMessage get() {
        FlexContainer container = Carousel.builder()
                .contents(trailerLinkBubbleList)
                .build();

        FlexMessage trailerLinkFlexMessage = FlexMessage.builder()
                .altText(DEFAULT_ALT_TEXT)
                .contents(container)
                .build();

        return trailerLinkFlexMessage;
    }
}
