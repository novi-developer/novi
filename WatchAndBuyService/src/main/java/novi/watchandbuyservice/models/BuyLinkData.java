package novi.watchandbuyservice.models;

public class BuyLinkData {
    private String buyTitle;
    private String buyURL;
    private String buyImageURL;

    public BuyLinkData(String buyTitle, String buyURL, String buyImageURL) {
        this.buyTitle = buyTitle;
        this.buyURL = buyURL;
        this.buyImageURL = buyImageURL;
    }

    public String getBuyTitle() {
        return buyTitle;
    }

    public String getBuyURL() {
        return buyURL;
    }

    public String getBuyImageURL() {
        return buyImageURL;
    }
}
