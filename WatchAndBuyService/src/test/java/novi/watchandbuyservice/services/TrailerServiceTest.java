package novi.watchandbuyservice.services;

import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TrailerServiceTest {
    @Autowired
    private TrailerService trailerService;

    @Autowired
    private RestTemplate restTemplate;

    private String expectedJson;
    private MockRestServiceServer mockServer;

    @Value("${youtube.api.key}")
    private String YOUTUBE_API_KEY;

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        expectedJson = "{\n" +
                "  \"items\": [\n" +
                "    {\n" +
                "      \"id\": {\n" +
                "        \"videoId\": \"TcMBFSGVi1c\"\n" +
                "      },\n" +
                "      \"snippet\": {\n" +
                "        \"title\": \"Marvel Studios &#39; Avengers: Endgame - Official Trailer\",\n" +
                "        \"thumbnails\": {\n" +
                "          \"default\": {\n" +
                "            \"url\" : \"www.abalabal.com\" \n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": {\n" +
                "        \"videoId\": \"ZoRACE_7-LU\"\n" +
                "      },\n" +
                "      \"snippet\": {\n" +
                "        \"title\": \"AVENGERS ENDGAME Final Trailer (2019) Marvel Movie HD\",\n" +
                "        \"thumbnails\": {\n" +
                "          \"default\": {\n" +
                "            \"url\" : \"www.abalabal.com\" \n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": {\n" +
                "        \"videoId\": \"wuOvmyuYFMo\"\n" +
                "      },\n" +
                "      \"snippet\": {\n" +
                "        \"title\": \"Marvel Studios’ Avengers: Endgame- Official Trailer - UK Marvel | HD\",\n" +
                "        \"thumbnails\": {\n" +
                "          \"default\": {\n" +
                "            \"url\" : \"www.abalabal.com\" \n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";
    }


    @Test
    public void testTrailerServiceReturnAMessage() {
        mockServer.expect(ExpectedCount.once(),
            requestTo("https://www.googleapis.com/youtube/v3/search?part=snippet&key="
                      + YOUTUBE_API_KEY
                      + "&maxResults=3&order=relevance&q=avengers%20endgame%20official%20trailer&type=video"))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(expectedJson)
        );
        String argument = "avengers endgame";
        Object trailerServiceReturnedObject = trailerService.handle(argument);
        assertThat(trailerServiceReturnedObject, instanceOf(Message.class));
        assertThat(trailerServiceReturnedObject.toString().toLowerCase(), containsString("avengers"));
    }
}
