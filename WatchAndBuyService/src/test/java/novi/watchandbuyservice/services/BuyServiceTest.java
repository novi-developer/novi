package novi.watchandbuyservice.services;

import com.linecorp.bot.model.message.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class BuyServiceTest {
    @Autowired
    private BuyService buyService;

    @Test
    public void testBuyServiceReturnAMessage() {
        String argument = "avengers endgame";
        Object trailerServiceReturnedObject = buyService.handle(argument);
        assertThat(trailerServiceReturnedObject, instanceOf(Message.class));
    }
}
