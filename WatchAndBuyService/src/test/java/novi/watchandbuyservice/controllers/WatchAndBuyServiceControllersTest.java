package novi.watchandbuyservice.controllers;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.watchandbuyservice.clients.BotServiceClient;
import novi.watchandbuyservice.models.NoviReplyMessage;
import novi.watchandbuyservice.services.BuyService;
import novi.watchandbuyservice.services.TrailerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class WatchAndBuyServiceControllersTest {
    @InjectMocks
    @Spy
    private WatchAndBuyServiceControllers watchAndBuyServiceControllers;

    @Mock
    private TrailerService trailerService;

    @Mock
    private BuyService buyService;

    @Mock
    private BotServiceClient botServiceClient;

    private Message trailerServiceHandleReturnThis;
    private Message buyServiceHandleReturnThis;

    @Value("${youtube.api.key}")
    private String YOUTUBE_API_KEY;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        trailerServiceHandleReturnThis = new TextMessage("Marvel Studios’ Avengers: Endgame - Official Trailer - UK Marvel | HD\n"
                                                        + "www.youtube.com/watch?v=wuOvmyuYFMo\n");
        buyServiceHandleReturnThis = new TextMessage("Avengers : Endgame\nwww.amazon.com");
    }

    @Test
    public void testCanHandleTrailerQuery() {
        String query = "/trailer";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        watchAndBuyServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doReturn(trailerServiceHandleReturnThis).when(trailerService).handle(argument);
        Mockito.doNothing().when(watchAndBuyServiceControllers).sendMessage(replyToken, trailerServiceHandleReturnThis);
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));

        verify(watchAndBuyServiceControllers, times(1)).sendMessage(any(), any());
    }

    @Test
    public void testCanHandleBuyQuery() {
        String query = "/buy";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        watchAndBuyServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doReturn(buyServiceHandleReturnThis).when(buyService).handle(argument);
        Mockito.doNothing().when(watchAndBuyServiceControllers).sendMessage(replyToken, buyServiceHandleReturnThis);
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));

        verify(watchAndBuyServiceControllers, times(1)).sendMessage(any(), any());
    }

    @Test
    public void testCannotHandleQueryThatIsNotListed() {
        String query = "/randomquery";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";
        
        watchAndBuyServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(watchAndBuyServiceControllers).sendMessage(any(), any());
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));
        
        verify(watchAndBuyServiceControllers, times(1)).sendMessage(any(), any());
    }
}
