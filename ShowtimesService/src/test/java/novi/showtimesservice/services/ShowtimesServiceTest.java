package novi.showtimesservice.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ShowtimesServiceTest {
    @Autowired
    private ShowtimesService showtimesService;

    @Autowired
    private RestTemplate restTemplate;

    private String expectedJson;
    private MockRestServiceServer mockServer;

    @Value("${showtimes.api.key}")
    private String SHOWTIMES_API_KEY;

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        // expectedJson = "{\"items\":[{\"id\":{\"videoId\":\"TcMBFSGVi1c\"},\"snippet\":{\"title\":\"Marvel Studios" +
        //         "&#39; Avengers: Endgame - Official Trailer\"}},{\"id\":{\"videoId\":\"ZoRACE_7-LU\"},\"" +
        //         "snippet\":{\"title\":\"AVENGERS ENDGAME Final Trailer (2019) Marvel Movie HD\"}},{\"id\"" +
        //         ":{\"videoId\":\"wuOvmyuYFMo\"},\"snippet\":{\"title\":\"Marvel Studios’ Avengers: Endgame " +
        //         "- Official Trailer - UK Marvel | HD\"}}]}";
    }

    @Test
    public void testShowtimesServiceGetBioskopURL() {
        // String expectedURL = "https://jadwalnonton.com/bioskop/di-jakarta/cgv-grand-indonesia-jakarta-2.html";
        // String argument = "grand indonesia";
        // Object showtimesServiceReturnedObject = showtimesService.getUrlBioskop(argument);
        // System.out.println(showtimesService.getUrlBioskop(argument));
        // assertEquals(null, expectedURL, showtimesServiceReturnedObject);
        return;
    }

    @Test
    public void testShowtimesServiceReturnAMessage() {
        
        
        return;
    }

    // @Test
    // public void testTrailerServiceReturnAMessage() {
    //     mockServer.expect(ExpectedCount.once(),
    //         requestTo("https://www.googleapis.com/youtube/v3/search?part=snippet&key="
    //                   + YOUTUBE_API_KEY
    //                   + "&maxResults=3&order=relevance&q=avengers%20endgame%20official%20trailer&type=video"))
    //         .andExpect(method(HttpMethod.GET))
    //         .andRespond(withStatus(HttpStatus.OK)
    //         .contentType(MediaType.APPLICATION_JSON)
    //         .body(expectedJson)
    //     );
    //     String argument = "avengers endgame";
    //     Object trailerServiceReturnedObject = trailerService.handle(argument);
    //     assertThat(trailerServiceReturnedObject, instanceOf(Message.class));
    //     assertThat(trailerServiceReturnedObject.toString().toLowerCase(), containsString("avengers"));
    // }
}
