package novi.showtimesservice.controllers;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.showtimesservice.clients.BotServiceClient;
import novi.showtimesservice.models.NoviReplyMessage;
import novi.showtimesservice.services.ShowtimesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ShowtimesServiceControllersTest {
    @InjectMocks
    @Spy
    private ShowtimesServiceControllers showtimesServiceControllers;

    @Mock
    private ShowtimesService showtimesService;

    @Mock
    private BotServiceClient botServiceClient;

    private Message showtimesServiceHandleReturnThis;

    @Value("${showtimes.api.key}")
    private String SHOWTIMES_API_KEY;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        showtimesServiceHandleReturnThis = new TextMessage("AVENGERS: ENDGAME (2D Gold Class)\n"
                                                        + "https://jadwalnonton.com/film/2019/avengers-endgame/\n"
                                                        + "Action, Adventure, Fantasy - 181 Menit\n"
                                                        + "10:30 14:10 17:50 21:30 \n");
    }

    @Test
    public void testCanHandleShowtimesQuery() {
        String query = "/showtimes";
        String argument = "grand indonesia";
        String replyToken = "dummyReplyToken";

        showtimesServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doReturn(showtimesServiceHandleReturnThis).when(showtimesService).handle(argument);
        Mockito.doNothing().when(showtimesServiceControllers).sendMessage(replyToken, showtimesServiceHandleReturnThis);
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));

        verify(showtimesServiceControllers, times(1)).sendMessage(any(), any());
    }

    @Test
    public void testCannotHandleQueryThatIsNotListed() {
        String query = "/randomquery";
        String argument = "grand indonesia";
        String replyToken = "dummyReplyToken";
        
        showtimesServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(showtimesServiceControllers).sendMessage(any(), any());
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));
        
        verify(showtimesServiceControllers, times(1)).sendMessage(any(), any());
    }
}
