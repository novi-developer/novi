package novi.showtimesservice.services;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ShowtimesService {
    @Autowired
    private RestTemplate restClient;

    @Value("${showtimes.api.key}")
    private String showtimesAPIKey;

    private String accessV1QueryFormat = "https://rest.farzain.com/api/special/bioskop/" +
                                         "bioskop.php?id=%s&apikey=%s";

    private String accessV2QueryFormat = "https://rest.farzain.com/api/special/bioskop/" +
                                         "bioskop_result.php?apikey=%s&id=%s";


    private String border = "===============\n";

    private int MAX_REPLY = 5;

    public String getUrlBioskop(String argument) {
        String bioskopName = argument.replace(" ", "+");
        String bioskopQuery = String.format(accessV1QueryFormat, bioskopName, showtimesAPIKey);

        String bioskopQueryResult = restClient.getForObject(bioskopQuery, String.class);
        JSONParser jsonParser = new JSONParser();
        try {
            JSONArray jsonDaftarBioskop = (JSONArray) jsonParser.parse(bioskopQueryResult);
            JSONObject bioskop = (JSONObject) jsonDaftarBioskop.get(0);

            String bioskopUrl = (String) bioskop.get("url");

            return bioskopUrl;
        } catch (ParseException parseException) {
            return "";
        }
    }

    public Message handle(String argument) {
        String url = getUrlBioskop(argument);
        String showtimesQuery = String.format(accessV2QueryFormat, showtimesAPIKey, url);

        String showtimesQueryResult = restClient.getForObject(showtimesQuery, String.class);
        JSONParser jsonParser = new JSONParser();
        try {
            JSONArray daftarShowtimes = (JSONArray) jsonParser.parse(showtimesQueryResult);
            StringBuilder showtimesResult = new StringBuilder();
            if (daftarShowtimes.size() > MAX_REPLY) {
                daftarShowtimes.subList(MAX_REPLY, daftarShowtimes.size()).clear();
            }
            
            for (Object rawShowtime: daftarShowtimes) {
                JSONObject showtime = (JSONObject) rawShowtime;
                String title = (String) showtime.get("title");
                String filmUrl = (String) showtime.get("url");
                String category = (String) showtime.get("category");
                String jadwal = (String) showtime.get("Jadwal");

                showtimesResult.append(border);
                showtimesResult.append(title + "\n");
                showtimesResult.append(url + "\n");
                showtimesResult.append(category + "\n");
                showtimesResult.append(jadwal + "\n");
                showtimesResult.append(border);
            }
            return new TextMessage(showtimesResult.toString());
        } catch (ParseException parseException) {
            return new TextMessage("Pencarian jadwal tayang gagal.");
        }
    }
}
