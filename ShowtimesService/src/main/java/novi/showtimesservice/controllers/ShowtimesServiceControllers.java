package novi.showtimesservice.controllers;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.showtimesservice.clients.BotServiceClient;
import novi.showtimesservice.models.NoviReplyMessage;
import novi.showtimesservice.services.ShowtimesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableFeignClients
public class ShowtimesServiceControllers {
    @Autowired
    private ShowtimesService showtimesService;

    @Autowired
    private BotServiceClient botServiceClient;

    @GetMapping("/query")
    @ResponseStatus(value = HttpStatus.OK)
    public void handleQuery(@RequestParam("query") String query, @RequestParam("argument") String argument,
            @RequestParam("replyToken") String replyToken) {
        if (query.contains("/showtimes")) {
            Message message = showtimesService.handle(argument);
            this.sendMessage(replyToken, message);
        } else {
            TextMessage failureMessage = new TextMessage("Query tidak dapat diproses!");
            this.sendMessage(replyToken, failureMessage);
        }
    }

    public void sendMessage(String replyToken, Message message) {
        NoviReplyMessage replyMessage = new NoviReplyMessage(replyToken, message);
        botServiceClient.send(replyMessage);
    }
}
