package novi.recommendationandreviewservice.suppliers;

import novi.recommendationandreviewservice.models.TrailerLinkData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class TrailerLinkDataSupplier implements Supplier< List<TrailerLinkData> > {
    private static String TITLE_FORMAT = "Title: %s";
    private static String TRAILER_LINK_FORMAT = "https://youtube.com/watch?v=%s";

    private String youtubeQueryResult;

    public TrailerLinkDataSupplier(String youtubeQueryResult) {
        this.youtubeQueryResult = youtubeQueryResult;
    }

    @Override
    public List<TrailerLinkData> get() {
        JSONParser jsonParser = new JSONParser();
        List<TrailerLinkData> result = new ArrayList<>();
        try {
            JSONObject jsonYoutubeQueryResult = (JSONObject) jsonParser.parse(this.youtubeQueryResult);
            JSONArray daftarVideo = (JSONArray) jsonYoutubeQueryResult.get("items");

            for (Object rawVideo: daftarVideo) {
                JSONObject video = (JSONObject) rawVideo;
                JSONObject idInfo = (JSONObject) video.get("id");
                String videoURL = String.format(TRAILER_LINK_FORMAT, (String) idInfo.get("videoId"));

                JSONObject snippet = (JSONObject) video.get("snippet");
                String title = String.format(TITLE_FORMAT, (String) snippet.get("title"));

                JSONObject thumbnails = (JSONObject) snippet.get("thumbnails");
                JSONObject thumbnailDefault = (JSONObject) thumbnails.get("default");
                String urlThumbnail = (String) thumbnailDefault.get("url");

                result.add(new TrailerLinkData(videoURL, title, urlThumbnail));
            }
            return result;
        } catch (ParseException parseException) {
            return Collections.emptyList();
        }
    }
}
