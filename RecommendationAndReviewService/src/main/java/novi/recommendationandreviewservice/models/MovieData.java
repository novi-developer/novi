package novi.recommendationandreviewservice.models;

public class MovieData {
    private String videoURL;
    private String videoTitle;
    private String videoThumbnailURL;

    public MovieData(String videoURL, String videoTitle, String videoThumbnailURL) {
        this.videoURL = videoURL;
        this.videoTitle = videoTitle;
        this.videoThumbnailURL = videoThumbnailURL;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public String getVideoThumbnailURL() {
        return videoThumbnailURL;
    }
}
