package novi.recommendationandreviewservice.clients;

import novi.recommendationandreviewservice.models.NoviReplyMessage;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("BotService")
public interface BotServiceClient {
    @PostMapping("/send")
    void send(@RequestBody NoviReplyMessage replyMessage);
}
