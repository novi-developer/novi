package novi.recommendationandreviewservice.services;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RecommendationService {
    @Autowired
    private RestTemplate restClient;

    @Value("${tmdb.api.key}")
    private String tmdbAPIKey;// = "1b9807696d62067c561c4eef7e35bfb7";
    private String tmdbQueryFormat = "https://api.themoviedb.org/3/discover/movie" +
                                    "?api_key=" + tmdbAPIKey;
    
    public Message handle(String argument) {
        
        String tmdbQueryResult = restClient.getForObject(tmdbQueryFormat, String.class);
        
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject jsonTmdbQueryResult = (JSONObject) jsonParser.parse(tmdbQueryResult);
            JSONArray movieList = (JSONArray) jsonTmdbQueryResult.get("results");
            int MAX_MOVIES = 5;
            if (movieList.size() > MAX_MOVIES) {
                movieList.subList(MAX_MOVIES, movieList.size()).clear();
            }

            StringBuilder message = new StringBuilder();
            for (Object eachMovie : movieList) {
                JSONObject movie = (JSONObject) eachMovie;
                String title = (String) movie.get("title");
                String overview = (String) movie.get("overview");
                String rating = movie.get("vote_average").toString();
                message.append("Title: " + title + "\n");
                message.append("Synopsis: " + overview + "\n");
                message.append("TMDB Rating: " + rating + "\n");
                message.append("\n==============================\n");
            }
            return new TextMessage(message.toString());
        } catch (ParseException e) {
            return new TextMessage("Pencarian film gagal.");
        }
    }
}