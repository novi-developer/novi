package novi.recommendationandreviewservice.services;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RatingService {
    @Autowired
    private RestTemplate restClient;

    // @Value("${omdb.api.key}")
    private String omdbAPIKey = "e943aca8";
    private String omdbQueryFormat = "http://www.omdbapi.com/?t=%s" +
                                    "&api_key=" + omdbAPIKey;
    
    public Message handle(String argument) {
        String omdbQuery = String.format(omdbQueryFormat, argument);
        String omdbQueryResult = restClient.getForObject(omdbQuery, String.class);
        
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject jsonTmdbQueryResult = (JSONObject) jsonParser.parse(omdbQueryResult);
            JSONObject title = (JSONObject) jsonTmdbQueryResult.get("Title");
            JSONArray ratingList = (JSONArray) jsonTmdbQueryResult.get("Ratings");

            StringBuilder message = new StringBuilder();
            message.append("Title: " + title + "\n\n");
            for (Object eachRating : ratingList) {
                JSONObject rating = (JSONObject) eachRating;
                String source = (String) rating.get("source");
                String value = (String) rating.get("value");
                message.append("Source: " + source + "\n");
                message.append("Rating: " + value + "\n\n");
            }
            return new TextMessage(message.toString());
        } catch (ParseException e) {
            return new TextMessage("Pencarian film gagal.");
        }
    }
}