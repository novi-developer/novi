package novi.recommendationandreviewservice.services;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class ReviewService {
    @Autowired
    private RestTemplate restClient;

    @Value("${tmdb.api.key}")
    private String tmdbAPIKey;// = "1b9807696d62067c561c4eef7e35bfb7";
    private String reviewTmdbQueryFormat = "https://api.themoviedb.org/3/"
        + "movie/%s/reviews?api_key=" + tmdbAPIKey;
    private String searchTmdbQueryFormat = "https://api.themoviedb.org/3/"
        + "search/movie?api_key=" + tmdbAPIKey 
        + "&language=en-US&query=%s&page=1&include_adult=false";

    private int MAX_REVIEWS = 5;
    private int MAX_REVIEW_LENGTH = 300;
    
    public Message handle(String argument) {
        String tmdbQueryResult = restClient.getForObject(String.format(reviewTmdbQueryFormat, getMovieId(argument)), String.class);
        
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject jsonTmdbQueryResult = (JSONObject) jsonParser.parse(tmdbQueryResult);
            JSONArray reviewList = (JSONArray) jsonTmdbQueryResult.get("results");
            if (reviewList.size() > MAX_REVIEWS) {
                reviewList.subList(MAX_REVIEWS, reviewList.size()).clear();
            }

            StringBuilder message = new StringBuilder();
            for (Object eachReview : reviewList) {
                JSONObject review = (JSONObject) eachReview;
                String author = (String) review.get("author");
                String content = (String) review.get("content");
                
                if (content.length() > MAX_REVIEW_LENGTH) {
                    content = content.substring(0, MAX_REVIEW_LENGTH).concat("...");
                }
                message.append("\"" + content + "\"\n");
                message.append("- " + author + "\n\n");
            }
            return new TextMessage(message.toString());
        } catch (ParseException e) {
            return new TextMessage("Pencarian review gagal.");
        }
    }

    private String getMovieId(String argument) {
        if (StringUtils.isNumeric(argument)) {
            return argument;
        } else {
            String tmdbQueryResult = restClient.getForObject(String.format(searchTmdbQueryFormat, argument), String.class);
            JSONParser jsonParser = new JSONParser();
            try {
                JSONObject jsonTmdbQueryResult = (JSONObject) jsonParser.parse(tmdbQueryResult);
                JSONArray movieList = (JSONArray) jsonTmdbQueryResult.get("results");
                JSONObject movie = (JSONObject) movieList.get(0);
                return Long.toString((Long) movie.get("id"), 0);
            } catch (ParseException e) {
                return "";
            }
        }
    }
}