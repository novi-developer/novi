package novi.recommendationandreviewservice.controllers;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.recommendationandreviewservice.clients.BotServiceClient;
import novi.recommendationandreviewservice.models.NoviReplyMessage;
import novi.recommendationandreviewservice.services.RecommendationService;
import novi.recommendationandreviewservice.services.ReviewService;
import novi.recommendationandreviewservice.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@EnableFeignClients
public class RecommendationAndReviewServiceControllers {
    @Autowired
    private BotServiceClient botServiceClient;
    
    @Autowired
    private RecommendationService recommendationService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private RatingService ratingService;

    @GetMapping("/query")
    public Message handleQuery(@RequestParam("query") String query, @RequestParam("argument") String argument) {
        if (query.contains("/recommendation")) {
            return recommendationService.handle(argument);
        } else if (query.contains("/review")) {
            return reviewService.handle(argument);
        } else if (query.contains("/rating")) {
            return ratingService.handle(argument);
        } else {
            return new TextMessage("Query tidak dapat diproses!");
        }
    }

    @Async
    public void sendMessage(String replyToken, Message message) {
        NoviReplyMessage replyMessage = new NoviReplyMessage(replyToken, message);
        botServiceClient.send(replyMessage);
    }
}
