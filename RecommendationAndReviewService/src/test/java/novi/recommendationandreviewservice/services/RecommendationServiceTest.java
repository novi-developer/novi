package novi.recommendationandreviewservice.services;

import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RecommendationServiceTest {
    @Autowired
    private RecommendationService recommendationService;

    @Autowired
    private RestTemplate restTemplate;

    private String expectedJson;
    private MockRestServiceServer mockServer;

    @Value("${tmdb.api.key}")
    private String tmdbAPIKey;// = "1b9807696d62067c561c4eef7e35bfb7";
    private String tmdbQueryFormat = "https://api.themoviedb.org/3/discover/movie" +
                                    "?api_key=" + tmdbAPIKey;

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        expectedJson = "";
        // expectedJson = "{\"items\":[{\"id\":{\"videoId\":\"TcMBFSGVi1c\"},\"snippet\":{\"title\":\"Marvel Studios" +
        //         "&#39; Avengers: Endgame - Official Trailer\"}},{\"id\":{\"videoId\":\"ZoRACE_7-LU\"},\"" +
        //         "snippet\":{\"title\":\"AVENGERS ENDGAME Final Trailer (2019) Marvel Movie HD\"}},{\"id\"" +
        //         ":{\"videoId\":\"wuOvmyuYFMo\"},\"snippet\":{\"title\":\"Marvel Studios’ Avengers: Endgame " +
        //         "- Official Trailer - UK Marvel | HD\"}}]}";
    }


    @Test
    public void testRecommendationServiceReturnAMessage() {
        mockServer.expect(ExpectedCount.once(),
            requestTo(tmdbQueryFormat))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(expectedJson)
        );

        try {
            String argument = "";
            Object recommendationServiceReturnedObject = recommendationService.handle(argument);
            assertThat(recommendationServiceReturnedObject, instanceOf(Message.class));
            assertThat(recommendationServiceReturnedObject.toString().toLowerCase(), containsString("title:"));
            // assertThat(recommendationServiceReturnedObject.toString().toLowerCase(), containsString("pencarian film gagal."));
        } catch(ClassCastException e) {
            e.printStackTrace();
        } catch(NullPointerException e) {
            e.printStackTrace();
        }
    }
}
