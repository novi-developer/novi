package novi.recommendationandreviewservice.services;

import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ReviewServiceTest {
    @Autowired
    private ReviewService reviewService;

    @Autowired
    private RestTemplate restTemplate;

    private String expectedJson;
    private MockRestServiceServer mockServer;

    @Value("${tmdb.api.key}")
    private String tmdbAPIKey;// = "1b9807696d62067c561c4eef7e35bfb7";
    private String reviewTmdbQueryFormat = "https://api.themoviedb.org/3/"
        + "movie/%s/reviews?api_key=" + tmdbAPIKey;
    private String searchTmdbQueryFormat = "https://api.themoviedb.org/3/"
        + "search/movie?api_key=" + tmdbAPIKey 
        + "&language=en-US&query=%s&page=1&include_adult=false";
    private String ID_MOVIE = "299534";

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        expectedJson = "";
        // expectedJson = "{\"items\":[{\"id\":{\"videoId\":\"TcMBFSGVi1c\"},\"snippet\":{\"title\":\"Marvel Studios" +
        //         "&#39; Avengers: Endgame - Official Trailer\"}},{\"id\":{\"videoId\":\"ZoRACE_7-LU\"},\"" +
        //         "snippet\":{\"title\":\"AVENGERS ENDGAME Final Trailer (2019) Marvel Movie HD\"}},{\"id\"" +
        //         ":{\"videoId\":\"wuOvmyuYFMo\"},\"snippet\":{\"title\":\"Marvel Studios’ Avengers: Endgame " +
        //         "- Official Trailer - UK Marvel | HD\"}}]}";
    }


    @Test
    public void testReviewServiceReturnAMessage() {
        mockServer.expect(ExpectedCount.once(),
            requestTo(String.format(reviewTmdbQueryFormat, ID_MOVIE)))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(expectedJson)
            );
        try {
            String argument = ID_MOVIE;
            Object reviewServiceReturnedObject = reviewService.handle(argument);
            assertThat(reviewServiceReturnedObject, instanceOf(Message.class));
            assertThat(reviewServiceReturnedObject.toString().toLowerCase(), containsString("\""));
            // assertFalse(reviewServiceReturnedObject.toString().toLowerCase(), containsString("Pencarian film gagal."));
        } catch(ClassCastException e) {
            e.printStackTrace();
        } catch(NullPointerException e) {
            e.printStackTrace();
        }
    }
}
