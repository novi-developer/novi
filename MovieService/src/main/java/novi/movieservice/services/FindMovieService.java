package novi.movieservice.services;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import novi.movieservice.models.MovieLinkData;
import novi.movieservice.suppliers.MovieLinkBubbleSupplier;
import novi.movieservice.suppliers.MovieLinkDataSupplier;
import novi.movieservice.suppliers.MovieLinkFlexMessageSupplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class FindMovieService {
    @Autowired
    private RestTemplate restClient;

    @Value("${tmdb.api.key}")
    private String tmdbAPIKey; // aa0cda615b19a38b58e9efdf9ead0692
    private String tmdbQueryFormat = "https://api.themoviedb.org/3/search/movie?" + "api_key=%s&language=en-US&query=%s"
            + "&page=1&include_adult=false";

    public Message handle(String argument) {
        String movieQuery = String.format(tmdbQueryFormat, tmdbAPIKey, argument);
        String movieQueryResult = restClient.getForObject(movieQuery, String.class);
        MovieLinkDataSupplier movieLinkDataSupplier = new MovieLinkDataSupplier(movieQueryResult);
        List<MovieLinkData> movieLinkDataList = movieLinkDataSupplier.get();
        if (!movieLinkDataList.isEmpty()) {
            List<Bubble> movieLinkBubbleList = new ArrayList<>();
            for (MovieLinkData movieLinkData : movieLinkDataList) {
                MovieLinkBubbleSupplier movieLinkBubbleSupplier = new MovieLinkBubbleSupplier(movieLinkData);
                movieLinkBubbleList.add(movieLinkBubbleSupplier.get());
            }
            MovieLinkFlexMessageSupplier movieLinkFlexMessageSupplier = new MovieLinkFlexMessageSupplier(
                    movieLinkBubbleList);
            FlexMessage movieFlexMessage = movieLinkFlexMessageSupplier.get();
            return movieFlexMessage;
        } else {
            String failMessage = "Maaf film yang dicari tidak bisa ditemukan :((.";
            return new TextMessage(failMessage);
        }
    }
}