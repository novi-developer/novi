package novi.movieservice.services;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import novi.movieservice.models.DetailMovieLinkData;
import novi.movieservice.suppliers.DetailMovieLinkBubbleSupplier;
import novi.movieservice.suppliers.DetailMovieLinkDataSupplier;
import novi.movieservice.suppliers.DetailMovieLinkFlexMessageSupplier;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieDetailService {
    @Autowired
    private RestTemplate restClient;

    @Value("${tmdb.api.key}")
    private String tmdbAPIKey;
    private String tmdbQueryFormat = "https://api.themoviedb.org/3/search/movie?" + "api_key=%s&language=en-US&query=%s"
            + "&page=1&include_adult=false";

    public Message handle(String argument) {
        String movieQuery = String.format(tmdbQueryFormat, tmdbAPIKey, argument);
        String movieQueryResult = restClient.getForObject(movieQuery, String.class);
        DetailMovieLinkDataSupplier detailmovieLinkDataSupplier = new DetailMovieLinkDataSupplier(movieQueryResult);
        List<DetailMovieLinkData> detailmovieLinkDataList = detailmovieLinkDataSupplier.get();
        if (!detailmovieLinkDataList.isEmpty()) {
            List<Bubble> detailmovieLinkBubbleList = new ArrayList<>();
            for (DetailMovieLinkData movieLinkData : detailmovieLinkDataList) {
                DetailMovieLinkBubbleSupplier detailmovieLinkBubbleSupplier = new DetailMovieLinkBubbleSupplier(
                        movieLinkData);
                detailmovieLinkBubbleList.add(detailmovieLinkBubbleSupplier.get());
            }
            DetailMovieLinkFlexMessageSupplier movieLinkFlexMessageSupplier = new DetailMovieLinkFlexMessageSupplier(
                    detailmovieLinkBubbleList);
            FlexMessage movieFlexMessage = movieLinkFlexMessageSupplier.get();
            return movieFlexMessage;
        } else {
            String failMessage = "Maaf film yang dicari tidak bisa ditemukan :((.";
            return new TextMessage(failMessage);
        }
    }
}
