package novi.movieservice.suppliers;

import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import novi.movieservice.models.MovieLinkData;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class MovieLinkBubbleSupplier implements Supplier<Bubble> {
        private static String LINK_BUTTON_TEXT = "Ketuk untuk membuka TMDB";

        private MovieLinkData movieLinkData;

        public MovieLinkBubbleSupplier(MovieLinkData movieLinkData) {
                this.movieLinkData = movieLinkData;
        }

        @Override
        public Bubble get() {
                Image heroImage = Image.builder().url(this.movieLinkData.getmovieTMDBThumbnailURL())
                                .size(Image.ImageSize.FULL_WIDTH).aspectRatio(Image.ImageAspectRatio.R9TO16)
                                .aspectMode(Image.ImageAspectMode.Cover).build();

                Text titleText = Text.builder().text(this.movieLinkData.getmovieTitle()).weight(Text.TextWeight.BOLD)
                                .size(FlexFontSize.LG).build();

                Box bodyBox = Box.builder().layout(FlexLayout.VERTICAL).contents(asList(titleText)).build();

                Button linkButton = Button.builder().style(Button.ButtonStyle.LINK).height(Button.ButtonHeight.MEDIUM)
                                .action(new URIAction(LINK_BUTTON_TEXT, this.movieLinkData.getmovieTMDBURL())).build();

                Box footerBox = Box.builder().layout(FlexLayout.VERTICAL).contents(asList(linkButton)).build();

                Bubble movieLinkBubble = Bubble.builder().hero(heroImage).body(bodyBox).footer(footerBox).build();

                return movieLinkBubble;
        }
}
