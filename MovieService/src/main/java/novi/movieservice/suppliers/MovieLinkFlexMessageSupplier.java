package novi.movieservice.suppliers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.container.FlexContainer;

import java.util.List;
import java.util.function.Supplier;

public class MovieLinkFlexMessageSupplier implements Supplier<FlexMessage> {
    private static String DEFAULT_ALT_TEXT = "Movie Links";

    private List<Bubble> movieLinkBubbleList;

    public MovieLinkFlexMessageSupplier(List<Bubble> movieLinkBubbleList) {
        this.movieLinkBubbleList = movieLinkBubbleList;
    }

    @Override
    public FlexMessage get() {
        FlexContainer container = Carousel.builder().contents(movieLinkBubbleList).build();

        FlexMessage movieLinkFlexMessage = FlexMessage.builder().altText(DEFAULT_ALT_TEXT).contents(container).build();

        return movieLinkFlexMessage;
    }
}
