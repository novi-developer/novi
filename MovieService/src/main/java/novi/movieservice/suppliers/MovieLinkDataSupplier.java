package novi.movieservice.suppliers;

import novi.movieservice.models.MovieLinkData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class MovieLinkDataSupplier implements Supplier<List<MovieLinkData>> {
    private static String TITLE_FORMAT = "%s";
    private static String MOVIE_LINK_FORMAT = "https://www.themoviedb.org/movie/%s";
    private static String POSTER_FORMAT = "https://image.tmdb.org/t/p/original%s";

    private String tmdbQueryResult;

    public MovieLinkDataSupplier(String tmdbQueryResult) {
        this.tmdbQueryResult = tmdbQueryResult;
    }

    @Override
    public List<MovieLinkData> get() {
        JSONParser jsonParser = new JSONParser();
        List<MovieLinkData> result = new ArrayList<>();
        try {
            JSONObject jsonMovieQueryResult = (JSONObject) jsonParser.parse(this.tmdbQueryResult);
            JSONArray daftarMovie = (JSONArray) jsonMovieQueryResult.get("results");
            for (int i = 0; i < Math.min(3, daftarMovie.size()); i++) {
                JSONObject movie = (JSONObject) daftarMovie.get(i);
                Long tmdbId = (Long) movie.get("id");
                String tmdbIdString = Long.toString(tmdbId);
                String title = (String) movie.get("original_title");
                String posterLink = (String) movie.get("poster_path");

                String movieTMDBURL = String.format(MOVIE_LINK_FORMAT, tmdbIdString);
                String movieTitle = String.format(TITLE_FORMAT, title);
                String movieTMDBThumbnailURL = String.format(POSTER_FORMAT, posterLink);
                result.add(new MovieLinkData(movieTMDBURL, movieTitle, movieTMDBThumbnailURL));
            }
            return result;
        } catch (ParseException parseException) {
            return Collections.emptyList();
        }
    }
}
