package novi.movieservice.suppliers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.container.FlexContainer;

import java.util.List;
import java.util.function.Supplier;

public class DetailMovieLinkFlexMessageSupplier implements Supplier<FlexMessage> {
    private static String DEFAULT_ALT_TEXT = "Movie Detail Links";

    private List<Bubble> detailMovieLinkBubbleList;

    public DetailMovieLinkFlexMessageSupplier(List<Bubble> detailMovieLinkBubbleList) {
        this.detailMovieLinkBubbleList = detailMovieLinkBubbleList;
    }

    @Override
    public FlexMessage get() {
        FlexContainer container = Carousel.builder().contents(detailMovieLinkBubbleList).build();

        FlexMessage detailmovieLinkFlexMessage = FlexMessage.builder().altText(DEFAULT_ALT_TEXT).contents(container)
                .build();

        return detailmovieLinkFlexMessage;
    }
}
