package novi.movieservice.suppliers;

import com.linecorp.bot.model.message.TextMessage;
import novi.movieservice.models.DetailMovieLinkData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class DetailMovieLinkDataSupplier implements Supplier<List<DetailMovieLinkData>> {
    private static String TITLE_FORMAT = "%s";
    private static String MOVIE_LINK_FORMAT = "https://www.themoviedb.org/movie/%s";
    private static String SYNOPSIS_FORMAT = "%s";
    private static String RELEASE_DATE_FORMAT = "%s";
    private static String POSTER_FORMAT = "https://image.tmdb.org/t/p/original%s";

    private String tmdbQueryResult;

    public DetailMovieLinkDataSupplier(String tmdbQueryResult) {
        this.tmdbQueryResult = tmdbQueryResult;
    }

    @Override
    public List<DetailMovieLinkData> get() {
        JSONParser jsonParser = new JSONParser();
        List<DetailMovieLinkData> result = new ArrayList<>();
        try {
            JSONObject jsonMovieQueryResult = (JSONObject) jsonParser.parse(this.tmdbQueryResult);
            JSONArray daftarMovie = (JSONArray) jsonMovieQueryResult.get("results");

            for (int i = 0; i < Math.min(3, daftarMovie.size()); i++) {
                JSONObject movie = (JSONObject) daftarMovie.get(i);
                Long tmdbId = (Long) movie.get("id");
                String tmdbIdString = Long.toString(tmdbId);
                String title = (String) movie.get("original_title");
                String posterLink = (String) movie.get("poster_path");
                String synopsisTemp = (String) movie.get("overview");
                String releaseDateTemp = (String) movie.get("release_date");

                String movieTMDBURL = String.format(MOVIE_LINK_FORMAT, tmdbIdString);
                String movieTitle = String.format(TITLE_FORMAT, title);
                String movieTMDBThumbnailURL = String.format(POSTER_FORMAT, posterLink);
                String synopsis = String.format(SYNOPSIS_FORMAT, synopsisTemp);
                String releaseDate = String.format(RELEASE_DATE_FORMAT, releaseDateTemp);
                result.add(new DetailMovieLinkData(movieTMDBURL, movieTitle, movieTMDBThumbnailURL, synopsis,
                        releaseDate));
            }
            return result;
        } catch (ParseException parseException) {
            return Collections.emptyList();
        }
    }
}
