package novi.movieservice.suppliers;

import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import novi.movieservice.models.DetailMovieLinkData;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class DetailMovieLinkBubbleSupplier implements Supplier<Bubble> {
        private static String LINK_BUTTON_TEXT = "Ketuk untuk membuka TMDB";

        private DetailMovieLinkData detailMovieLinkData;

        public DetailMovieLinkBubbleSupplier(DetailMovieLinkData detailMovieLinkData) {
                this.detailMovieLinkData = detailMovieLinkData;
        }

        @Override
        public Bubble get() {
                Image heroImage = Image.builder().url(this.detailMovieLinkData.getmovieTMDBThumbnailURL())
                                .size(Image.ImageSize.FULL_WIDTH).aspectRatio(Image.ImageAspectRatio.R9TO16)
                                .aspectMode(Image.ImageAspectMode.Cover).build();

                Text titleText = Text.builder().text(this.detailMovieLinkData.getmovieTitle())
                                .weight(Text.TextWeight.BOLD).size(FlexFontSize.LG).build();
                Box info = createInfoBox();
                Box bodyBox = Box.builder().layout(FlexLayout.VERTICAL).contents(asList(titleText, info)).build();

                Button linkButton = Button.builder().style(Button.ButtonStyle.LINK).height(Button.ButtonHeight.MEDIUM)
                                .action(new URIAction(LINK_BUTTON_TEXT, this.detailMovieLinkData.getmovieTMDBURL()))
                                .build();

                Box footerBox = Box.builder().layout(FlexLayout.VERTICAL).contents(asList(linkButton)).build();

                Bubble movieLinkBubble = Bubble.builder().hero(heroImage).body(bodyBox).footer(footerBox).build();

                return movieLinkBubble;
        }

        public Box createInfoBox() {
                final Box synopsis = Box.builder().layout(FlexLayout.BASELINE).spacing(FlexMarginSize.SM)
                                .contents(asList(
                                                Text.builder().text("Synopsis").color("#aaaaaa").size(FlexFontSize.SM)
                                                                .flex(1).build(),
                                                Text.builder().text(this.detailMovieLinkData.getsynopsis()).wrap(true)
                                                                .color("#666666").size(FlexFontSize.SM).flex(5)
                                                                .build()))
                                .build();
                final Box releaseDate = Box.builder().layout(FlexLayout.BASELINE).spacing(FlexMarginSize.SM)
                                .contents(asList(
                                                Text.builder().text("Release Date").color("#aaaaaa")
                                                                .size(FlexFontSize.SM).flex(1).build(),
                                                Text.builder().text(this.detailMovieLinkData.getreleaseDate())
                                                                .wrap(true).color("#666666").size(FlexFontSize.SM)
                                                                .flex(5).build()))
                                .build();

                return Box.builder().layout(FlexLayout.VERTICAL).margin(FlexMarginSize.LG).spacing(FlexMarginSize.SM)
                                .contents(asList(synopsis, releaseDate)).build();
        }
}
