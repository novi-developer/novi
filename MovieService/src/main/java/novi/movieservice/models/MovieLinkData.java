package novi.movieservice.models;

public class MovieLinkData {
    private String movieTMDBURL;
    private String movieTitle;
    private String movieTMDBThumbnailURL;

    public MovieLinkData(String movieTMDBURL, String movieTitle, String movieTMDBThumbnailURL) {
        this.movieTMDBURL = movieTMDBURL;
        this.movieTitle = movieTitle;
        this.movieTMDBThumbnailURL = movieTMDBThumbnailURL;
    }

    public String getmovieTMDBURL() {
        return movieTMDBURL;
    }

    public String getmovieTitle() {
        return movieTitle;
    }

    public String getmovieTMDBThumbnailURL() {
        return movieTMDBThumbnailURL;
    }
}
