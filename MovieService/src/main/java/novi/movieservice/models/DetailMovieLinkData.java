package novi.movieservice.models;

public class DetailMovieLinkData {
    private String movieTMDBURL;
    private String movieTitle;
    private String movieTMDBThumbnailURL;
    private String synopsis;
    private String releaseDate;

    public DetailMovieLinkData(String movieTMDBURL, String movieTitle, String movieTMDBThumbnailURL, String synopsis,
            String releaseDate) {
        this.movieTMDBURL = movieTMDBURL;
        this.movieTitle = movieTitle;
        this.movieTMDBThumbnailURL = movieTMDBThumbnailURL;
        this.synopsis = synopsis;
        this.releaseDate = releaseDate;
    }

    public String getmovieTMDBURL() {
        return movieTMDBURL;
    }

    public String getmovieTitle() {
        return movieTitle;
    }

    public String getmovieTMDBThumbnailURL() {
        return movieTMDBThumbnailURL;
    }

    public String getsynopsis() {
        return synopsis;
    }

    public String getreleaseDate() {
        return releaseDate;
    }

}
