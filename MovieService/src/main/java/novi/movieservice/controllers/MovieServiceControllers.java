package novi.movieservice.controllers;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.movieservice.clients.BotServiceClient;
import novi.movieservice.models.NoviReplyMessage;
import novi.movieservice.services.FindMovieService;
import novi.movieservice.services.MovieDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableFeignClients
public class MovieServiceControllers {
    @Autowired
    private MovieDetailService movieDetail;

    @Autowired
    private FindMovieService findMovie;

    @Autowired
    private BotServiceClient botServiceClient;

    @GetMapping("/query")
    @ResponseStatus(value = HttpStatus.OK)
    public void handleQuery(@RequestParam("query") String query, @RequestParam("argument") String argument, @RequestParam("replyToken") String replyToken) {
        if (query.contains("/find")) {
            Message message = findMovie.handle(argument);
            this.sendMessage(replyToken, message);
        } else if (query.contains("/moviedetail")) {
            Message message = movieDetail.handle(argument);
            this.sendMessage(replyToken, message);
        } else {
            TextMessage failureMessage = new TextMessage("Query tidak dapat diproses!");
            this.sendMessage(replyToken, failureMessage);
        }
    }

    @Async
    public void sendMessage(String replyToken, Message message) {
        NoviReplyMessage replyMessage = new NoviReplyMessage(replyToken, message);
        botServiceClient.send(replyMessage);
    }
}
