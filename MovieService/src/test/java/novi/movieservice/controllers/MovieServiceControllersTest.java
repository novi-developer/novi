package novi.movieservice.controllers;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.movieservice.clients.BotServiceClient;
import novi.movieservice.models.NoviReplyMessage;
import novi.movieservice.services.FindMovieService;
import novi.movieservice.services.MovieDetailService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class MovieServiceControllersTest {
    @InjectMocks
    @Spy
    private MovieServiceControllers movieServiceControllers;

    @Mock
    private FindMovieService findMovieService;

    @Mock
    private MovieDetailService movieDetailService;

    @Mock
    private BotServiceClient botServiceClient;

    private Message movieDetailServiceHandleReturnThis;
    private Message findMovieServiceHandleReturnThis;

    @Value("${tmdb.api.key}")
    private String TMDB_API_KEY;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        movieDetailServiceHandleReturnThis = new TextMessage("Title: Dealova\n"
                + "https://image.tmdb.org/t/p/originalnull\n"
                + "Synopsis: A high school girl is torn between two young men, each loving her in their own way.\n"
                + "Release date: 2005-09-08\n" + "Link: https://www.themoviedb.org/movie/343015");
        findMovieServiceHandleReturnThis = new TextMessage(
                "Title: Dealova\n" + "Link: https://www.themoviedb.org/movie/343015");
    }

    @Test
    public void testCanHandleFindQuery() {
        String query = "/find";
        String argument = "dealova";
        String replyToken = "dummyReplyToken";

        movieServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doReturn(findMovieServiceHandleReturnThis).when(findMovieService).handle(argument);
        Mockito.doNothing().when(movieServiceControllers).sendMessage(replyToken, findMovieServiceHandleReturnThis);
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));

        verify(movieServiceControllers, times(1)).sendMessage(any(), any());
    }

    @Test
    public void testCanHandleMoviedetailQuery() {
        String query = "/moviedetail";
        String argument = "dealova";
        String replyToken = "dummyReplyToken";

        movieServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doReturn(movieDetailServiceHandleReturnThis).when(movieDetailService).handle(argument);
        Mockito.doNothing().when(movieServiceControllers).sendMessage(replyToken, movieDetailServiceHandleReturnThis);
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));

        verify(movieServiceControllers, times(1)).sendMessage(any(), any());
    }

    @Test
    public void testCannotHandleQueryThatIsNotListed() {
        String query = "/randomquery";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        movieServiceControllers.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(movieServiceControllers).sendMessage(any(), any());
        Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));

        verify(movieServiceControllers, times(1)).sendMessage(any(), any());
    }
}
