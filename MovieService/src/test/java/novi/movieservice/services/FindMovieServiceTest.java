package novi.movieservice.services;

import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class FindMovieServiceTest {
    @Autowired
    private FindMovieService FindMovieService;

    @Autowired
    private RestTemplate restTemplate;

    private String expectedJson;
    private MockRestServiceServer mockServer;

    @Value("${tmdb.api.key}")
    private String TMDB_API_KEY;

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        expectedJson = "{\"page\": 1,\"total_results\": 1,\"total_pages\": 1,\"results\": [{\"vote_count\": 2,"
                + "\"id\": 343015,\"video\": false,\"vote_average\": 6.3,\"title\": \"Dealova\",\"popularity\": 0.6,"
                + "\"poster_path\": \"/b4gv5KCitlvRLIey1IvVnlNDSfw.jpg\",\"original_language\": \"id\","
                + "\"original_title\": \"Dealova\",\"genre_ids\": [18,10749],\"backdrop_path\": null,"
                + "\"adult\": false,\"overview\": \"A high school girl is torn between two young men, each loving her in their own way.\","
                + "\"release_date\": \"2005-09-08\"}]}";
    }

    @Test
    public void testFindMovieServiceReturnAMessage() {
        mockServer
                .expect(ExpectedCount.once(),
                        requestTo("https://api.themoviedb.org/3/search/movie?api_key=" + TMDB_API_KEY
                                + "&language=en-US&query=dealova&page=1&include_adult=false"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(expectedJson));
        String argument = "dealova";
        Object findMovieServiceReturnedObject = FindMovieService.handle(argument);
        assertThat(findMovieServiceReturnedObject, instanceOf(Message.class));
        assertThat(findMovieServiceReturnedObject.toString().toLowerCase(), containsString("dealova"));
    }
}