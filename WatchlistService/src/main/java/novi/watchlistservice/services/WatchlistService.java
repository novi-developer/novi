package novi.watchlistservice.services;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WatchlistService {
    @Autowired
    private RestTemplate restClient;

    @Value("${tmdb.api.key}")
    private String tmdbAPIKey; // 9a70f1ef0fbbf384101f549cc25d7d9c 
    private String movieRequestFormat = "https://api.themoviedb.org/3/movie/%s?api_key=%s&language=en-US";

    private String bubleTitle = "Favorite Movies\n";
    private String borderUpest = "**************\n\n";
    private String listFormat = "• %s\n";

    public Message handle(String argument) {
        String requestMovie = String.format(movieRequestFormat, argument, tmdbAPIKey);
        String requestMovieResult = restClient.getForObject(requestMovie, String.class);
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject movie = (JSONObject) jsonParser.parse(requestMovieResult);
            StringBuilder tmdbLinks = new StringBuilder();
            tmdbLinks.append(bubleTitle);
            tmdbLinks.append(borderUpest);

            String title = (String) movie.get("original_title");
            tmdbLinks.append(String.format(listFormat, title));

            return new TextMessage(tmdbLinks.toString());
        } catch (ParseException parseException) {
            return new TextMessage("Film gagal ditambahkan ke favorit :<");
        }
    }
}