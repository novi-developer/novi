// package novi.watchlistservice.controllers;

// import com.linecorp.bot.model.ReplyMessage;
// import com.linecorp.bot.model.message.Message;
// import com.linecorp.bot.model.message.TextMessage;
// import novi.watchlistservice.clients.BotServiceClient;
// import novi.watchlistservice.models.NoviReplyMessage;
// import novi.watchlistservice.services.WatchlistService;
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.mockito.*;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.http.HttpMethod;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.MediaType;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.web.client.ExpectedCount;
// import org.springframework.test.web.client.MockRestServiceServer;
// import org.springframework.web.client.RestTemplate;

// import static org.hamcrest.CoreMatchers.instanceOf;
// import static org.junit.Assert.assertEquals;
// import static org.junit.Assert.assertThat;
// import static org.mockito.ArgumentMatchers.any;
// import static org.mockito.Mockito.doNothing;
// import static org.mockito.Mockito.mock;
// import static org.mockito.Mockito.times;
// import static org.mockito.Mockito.verify;
// import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
// import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
// import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

// @SpringBootTest
// @RunWith(SpringJUnit4ClassRunner.class)
// public class WatchlistServiceControllersTest {
//     @InjectMocks
//     @Spy
//     private WatchlistServiceControllers watchlistServiceControllers;

//     @Mock
//     private WatchlistService watchlistService;

//     @Mock
//     private BotServiceClient botServiceClient;

//     private Message watchlistServiceHandleReturnThis;

// 	@Value("${tmdb.api.key}")
//     private String TMDB_API_KEY;

//     @Before
//     public void init() {
//         MockitoAnnotations.initMocks(this);
//         watchlistServiceHandleReturnThis = new TextMessage("• Black Swan");
//     }

//     @Test
//     public void testCanHandleQuery() {
//         String query = "/addfavorite";
//         String argument = "44214";
//         String replyToken = "dummyReplyToken";

//         watchlistServiceControllers.handleQuery(query, argument, replyToken);
//         Mockito.doReturn(watchlistServiceHandleReturnThis).when(watchlistService).handle(argument);
//         Mockito.doNothing().when(watchlistServiceControllers).sendMessage(replyToken, watchlistServiceHandleReturnThis);
//         Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));

//         verify(watchlistServiceControllers, times(1)).sendMessage(any(), any());
//     }

//     @Test
//     public void testCannotHandleQueryThatIsNotListed() {
//         String query = "/syalala";
//         String argument = "277834";
//         String replyToken = "dummyReplyToken";
        
//         watchlistServiceControllers.handleQuery(query, argument, replyToken);
//         Mockito.doNothing().when(watchlistServiceControllers).sendMessage(any(), any());
//         Mockito.doNothing().when(botServiceClient).send(any(NoviReplyMessage.class));
        
//         verify(watchlistServiceControllers, times(1)).sendMessage(any(), any());
//     }
// }
