// package novi.watchlistservice.services;

// import com.linecorp.bot.model.message.Message;
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.mockito.Mock;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.http.HttpMethod;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.MediaType;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.web.client.ExpectedCount;
// import org.springframework.test.web.client.MockRestServiceServer;
// import org.springframework.web.client.RestTemplate;

// import static org.hamcrest.CoreMatchers.containsString;
// import static org.hamcrest.CoreMatchers.instanceOf;
// import static org.junit.Assert.assertThat;
// import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
// import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
// import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

// @SpringBootTest
// @RunWith(SpringJUnit4ClassRunner.class)
// public class WatchlistServiceTest {
//     @Autowired
//     private WatchlistService watchlistService;

//     @Autowired
//     private RestTemplate restTemplate;

//     @Mock
//     private MockRestServiceServer mockServer;

//     private String expectedJson;

//     @Value("${tmdb.api.key}")
//     private String TMDB_API_KEY;

//     @Before
//     public void init() {
//         mockServer = MockRestServiceServer.createServer(restTemplate);
//         expectedJson = "{\"adult\":false," +
//                         "\"backdrop_path\":\"/hbn46fQaRmlpBuUrEiFqv0GDL6Y.jpg\"," +
//                         "\"belongs_to_collection\":" + 
//                         "{\"id\":86311," +
//                         "\"name\":\"The Avengers Collection\"," + 
//                         "\"poster_path\":\"/qJawKUQcIBha507UahUlX0keOT7.jpg\"" +
//                         "\"backdrop_path\":\"/zuW6fOiusv4X9nnW3paHGfXcSll.jpg\"}," + 
//                         "\"budget\":220000000," + 
//                         "\"genres\":" +
//                         "[{\"id\":878,\"name\":\"Science Fiction\"}," +
//                         "{\"id\":28,\"name\":\"Action\"}," +
//                         "{\"id\":12,\"name\":\"Adventure\"}]," + 
//                         "\"homepage\":\"http://marvel.com/avengers_movie/\"," + 
//                         "\"id\":24428," + 
//                         "\"imdb_id\":\"tt0848228\"," +
//                         "\"original_language\":\"en\"," +
//                         "\"original_title\":\"The Avengers\"," +
//                         "\"overview\":\"When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!\"," +
//                         "\"popularity\":125.331," + 
//                         "\"poster_path\":\"/cezWGskPY5x7GaglTTRN4Fugfb8.jpg\"," + 
//                         "\"production_companies\":" + 
//                         "{\"id\":420," +
//                         "\"logo_path\":\"/hUzeosd33nzE5MCNsZxCGEKTXaQ.png\"," +
//                         "\"name\":\"Marvel Studios\"," + 
//                         "\"origin_country\":\"US\"}]," +
//                         "\"production_countries\":" +
//                         "[{\"iso_3166_1\":\"US\"," +
//                         "\"name\":\"United States of America\"}]," + 
//                         "\"release_date\":\"2012-04-25\"," +
//                         "\"revenue\":1519557910," +
//                         "\"runtime\":143," +
//                         "\"spoken_languages\":" + 
//                             "[{\"iso_639_1\":\"en\",\"name\":\"English\"}," +
//                             "{\"iso_639_1\":\"hi\",\"name\":\"??????\"}," +
//                             "{\"iso_639_1\":\"ru\",\"name\":\"P??????\"}]," + 
//                         "\"status\":\"Released\"," +
//                         "\"tagline\":\"Some assembly required.\"," +
//                         "\"title\":\"The Avengers\"," +
//                         "\"video\":false," +
//                         "\"vote_average\":7.6," +
//                         "\"vote_count\":19222}";
//     }

//     @Test
//     public void testFindMovieServiceReturnAMessage() {
//         mockServer
//                 .expect(ExpectedCount.once(),
//                         requestTo("https://api.themoviedb.org/3/movie/24428?api_key=" + TMDB_API_KEY + "&language=en-US"))
//                 .andExpect(method(HttpMethod.GET))
//                 .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(expectedJson));
//         String argument = "24428";
//         Object WatchlistServiceReturnedObject = watchlistService.handle(argument);
//         assertThat(WatchlistServiceReturnedObject, instanceOf(Message.class));
//         assertThat(WatchlistServiceReturnedObject.toString().toLowerCase(), containsString("The Avengers"));
//     }
// }