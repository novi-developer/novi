package novi.eurekaregistryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableEurekaServer
@SpringBootApplication
@EnableAsync
public class EurekaRegistryServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(EurekaRegistryServiceApp.class, args);
    }
}
