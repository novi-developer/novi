package novi.botservice.services;

import com.linecorp.bot.model.message.Message;
import novi.botservice.clients.WatchAndBuyServiceClient;
import novi.botservice.handlers.QueryHandler;
import novi.botservice.handlers.WatchAndBuyQueryHandler;
import novi.botservice.models.AcceptedQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class HelpServiceTest {
    @Mock
    private WatchAndBuyServiceClient watchAndBuyServiceClient;

    private QueryHandler queryHandler;

    private HelpService helpService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/trailer",
                "Mencari trailer berdasarkan nama film. Contoh : /trailer avengers endgame"
        ));
        acceptedQueries.add(new AcceptedQuery(
                "/buy",
                "Mencari link untuk membeli film yang diinginkan di Google Play. Contoh : /buy zootopia"
        ));
        queryHandler = new WatchAndBuyQueryHandler(acceptedQueries, null, watchAndBuyServiceClient);
        helpService = new HelpService();
    }

    @Test
    public void helpServiceCreateCorrectHelpMessage() {
        Object result = helpService.handle(queryHandler);
        assertThat(result, instanceOf(Message.class));
        assertThat(result.toString().toLowerCase(), containsString("/trailer"));
        assertThat(result.toString().toLowerCase(), containsString("/buy"));
    }
}
