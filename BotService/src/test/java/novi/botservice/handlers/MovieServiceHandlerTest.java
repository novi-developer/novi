package novi.botservice.handlers;

import novi.botservice.clients.MovieServiceClient;
import novi.botservice.exceptions.QueryCannotBeHandledException;
import novi.botservice.models.AcceptedQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class MovieServiceHandlerTest {
    @Mock
    private MovieServiceClient movieServiceClient;

    @InjectMocks
    private MovieServiceHandler movieServiceHandler;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/find",
                "Menunjukkan daftar film berdasarkan suatu keyword. Contoh : /find avengers"
        ));
        acceptedQueries.add(new AcceptedQuery(
                "/moviedetail",
                "Menampilkan sinopsis serta informasi mendetail mengenai suatu film. Contoh : /moviedetail zootopia"
        ));
        movieServiceHandler = new MovieServiceHandler(acceptedQueries, null, movieServiceClient);
    }

    @Test
    public void movieServiceHandlerCanHandleFindQuery() {
        String query = "/find";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        movieServiceHandler.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(movieServiceClient).handleQuery(query, argument, replyToken);

        verify(movieServiceClient, times(1)).handleQuery(query, argument, replyToken);
    }

    @Test
    public void movieServiceHandlerCanHandleMovieDetailQuery() {
        String query = "/moviedetail";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        movieServiceHandler.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(movieServiceClient).handleQuery(query, argument, replyToken);

        verify(movieServiceClient, times(1)).handleQuery(query, argument, replyToken);
    }

    @Test
    public void movieServiceHandlerProperlyThrowExceptionWhenQueryIsNotAcceptable() {
        String query = "/randomquery";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        exception.expect(QueryCannotBeHandledException.class);
        movieServiceHandler.handleQuery(query, argument, replyToken);

        verify(movieServiceClient, times(0)).handleQuery(query, argument, replyToken);
    }
}
