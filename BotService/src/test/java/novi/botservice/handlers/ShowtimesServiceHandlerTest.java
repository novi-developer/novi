package novi.botservice.handlers;

import novi.botservice.clients.ShowtimesServiceClient;
import novi.botservice.exceptions.QueryCannotBeHandledException;
import novi.botservice.models.AcceptedQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class ShowtimesServiceHandlerTest {
    @Mock
    private ShowtimesServiceClient showtimesServiceClient;

    @InjectMocks
    private ShowtimesServiceHandler showtimesServiceHandler;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/showtimes",
                "Menunjukkan daftar jam tayang suatu film di bioskop. Contoh : /showtimes interstellar"
        ));
        showtimesServiceHandler = new ShowtimesServiceHandler(acceptedQueries, null, showtimesServiceClient);
    }

    @Test
    public void showtimesServiceHandlerCanHandleShowtimesQuery() {
        String query = "/showtimes";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        showtimesServiceHandler.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(showtimesServiceClient).handleQuery(query, argument, replyToken);

        verify(showtimesServiceClient, times(1)).handleQuery(query, argument, replyToken);
    }

    @Test
    public void showtimesServiceHandlerProperlyThrowExceptionWhenQueryIsNotAcceptable() {
        String query = "/randomquery";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        exception.expect(QueryCannotBeHandledException.class);
        showtimesServiceHandler.handleQuery(query, argument, replyToken);

        verify(showtimesServiceClient, times(0)).handleQuery(query, argument, replyToken);
    }
}
