package novi.botservice.handlers;

import novi.botservice.clients.WatchlistServiceClient;
import novi.botservice.exceptions.QueryCannotBeHandledException;
import novi.botservice.models.AcceptedQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class WatchlistQueryHandlerTest {
    @Mock
    private WatchlistServiceClient watchlistServiceClient;

    @InjectMocks
    private WatchlistQueryHandler watchlistQueryHandler;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/addfavorite",
                "Menambahkan film ke daftar favorite pribadi."
        ));
        watchlistQueryHandler = new WatchlistQueryHandler(acceptedQueries, null, watchlistServiceClient);
    }

    @Test
    public void watchlistQueryHandlerCanHandleAddFavoriteQuery() {
        String query = "/addfavorite";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        watchlistQueryHandler.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(watchlistServiceClient).handleQuery(query, argument, replyToken);

        verify(watchlistServiceClient, times(1)).handleQuery(query, argument, replyToken);
    }

    @Test
    public void watchlistQueryHandlerProperlyThrowExceptionWhenQueryIsNotAcceptable() {
        String query = "/randomquery";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        exception.expect(QueryCannotBeHandledException.class);
        watchlistQueryHandler.handleQuery(query, argument, replyToken);

        verify(watchlistServiceClient, times(0)).handleQuery(query, argument, replyToken);
    }
}
