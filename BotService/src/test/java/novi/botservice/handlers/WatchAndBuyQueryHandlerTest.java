package novi.botservice.handlers;

import novi.botservice.clients.WatchAndBuyServiceClient;
import novi.botservice.exceptions.QueryCannotBeHandledException;
import novi.botservice.models.AcceptedQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class WatchAndBuyQueryHandlerTest {
    @Mock
    private WatchAndBuyServiceClient watchAndBuyServiceClient;

    @InjectMocks
    private WatchAndBuyQueryHandler watchAndBuyQueryHandler;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/trailer",
                "Mencari trailer berdasarkan nama film. Contoh : /trailer avengers endgame"
        ));
        acceptedQueries.add(new AcceptedQuery(
                "/buy",
                "Mencari link untuk membeli film yang diinginkan di Google Play. Contoh : /buy zootopia"
        ));
        watchAndBuyQueryHandler = new WatchAndBuyQueryHandler(acceptedQueries, null, watchAndBuyServiceClient);
    }

    @Test
    public void watchAndBuyQueryHandlerCanHandleTrailerQuery() {
        String query = "/trailer";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        watchAndBuyQueryHandler.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(watchAndBuyServiceClient).handleQuery(query, argument, replyToken);

        verify(watchAndBuyServiceClient, times(1)).handleQuery(query, argument, replyToken);
    }

    @Test
    public void watchAndBuyQueryHandlerCanHandleBuyQuery() {
        String query = "/buy";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        watchAndBuyQueryHandler.handleQuery(query, argument, replyToken);
        Mockito.doNothing().when(watchAndBuyServiceClient).handleQuery(query, argument, replyToken);

        verify(watchAndBuyServiceClient, times(1)).handleQuery(query, argument, replyToken);
    }

    @Test
    public void watchAndBuyQueryHandlerProperlyThrowExceptionWhenQueryIsNotAcceptable() {
        String query = "/randomquery";
        String argument = "avengers endgame";
        String replyToken = "dummyReplyToken";

        exception.expect(QueryCannotBeHandledException.class);
        watchAndBuyQueryHandler.handleQuery(query, argument, replyToken);

        verify(watchAndBuyServiceClient, times(0)).handleQuery(query, argument, replyToken);
    }
}
