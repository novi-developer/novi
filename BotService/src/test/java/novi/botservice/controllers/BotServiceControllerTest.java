package novi.botservice.controllers;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.TextMessage;
import novi.botservice.handlers.QueryHandler;
import novi.botservice.models.NoviReplyMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class BotServiceControllerTest {
    @Mock
    private QueryHandler queryHandler;

    @Mock
    private LineMessagingClient lineClient;

    @InjectMocks
    private BotServiceController botServiceController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void botServiceControllerCanReceiveQuery() {
        String query = "/trailer";
        String argument = "zootopia";
        String replyToken = "haha";
        TextMessageContent textMessage = new TextMessageContent("haha", query + " " + argument);
        MessageEvent<TextMessageContent> event = new MessageEvent(
                replyToken,
                new UserSource("userId"),
                textMessage,
                Instant.EPOCH
                );
        botServiceController.receiveQuery(event);
        Mockito.doNothing().when(queryHandler).handleQuery(query, argument, replyToken);

        verify(queryHandler, times(1)).handleQuery(query, argument, replyToken);
    }

    @Test
    public void botServiceControllerCanSendMessage() {
        TextMessage textMessage = new TextMessage("Test Message");
        NoviReplyMessage noviReplyMessage = new NoviReplyMessage("replyToken", textMessage);

        ReplyMessage expectedReplyMessage = new ReplyMessage(noviReplyMessage.getReplyToken(), noviReplyMessage.getMessage());

        botServiceController.sendMessage(noviReplyMessage);
        when(lineClient.replyMessage(expectedReplyMessage)).thenReturn(null);

        verify(lineClient, times(1)).replyMessage(expectedReplyMessage);
    }
}
