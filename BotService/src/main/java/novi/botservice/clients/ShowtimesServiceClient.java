package novi.botservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("ShowtimesService")
public interface ShowtimesServiceClient {
    @GetMapping("/query")
    void handleQuery(@RequestParam("query") String query, @RequestParam("argument") String argument, @RequestParam("replyToken") String replyToken);
}
