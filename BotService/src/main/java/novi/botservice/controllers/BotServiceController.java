package novi.botservice.controllers;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import novi.botservice.clients.MovieServiceClient;
import novi.botservice.clients.ShowtimesServiceClient;
import novi.botservice.clients.WatchAndBuyServiceClient;
import novi.botservice.clients.WatchlistServiceClient;
import novi.botservice.exceptions.QueryCannotBeHandledException;
import novi.botservice.handlers.*;
import novi.botservice.models.AcceptedQuery;
import novi.botservice.models.NoviReplyMessage;
import novi.botservice.services.HelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@LineMessageHandler
@RestController
@EnableFeignClients(basePackages = { "novi.botservice.clients" })
public class BotServiceController {
    @Autowired
    private WatchAndBuyServiceClient watchAndBuyServiceClient;

    @Autowired
    private MovieServiceClient movieServiceClient;

    @Autowired
    private ShowtimesServiceClient showtimesServiceClient;

    @Autowired
    private WatchlistServiceClient watchlistServiceClient;

    @Autowired
    private HelpService helpService;

    @Value("${line.bot.channelToken}")
    private String channelToken;

    private QueryHandler rootQueryHandler;
    private LineMessagingClient lineClient;

    @EventMapping
    public void receiveQuery(MessageEvent<TextMessageContent> event) {
        String replyToken = event.getReplyToken();
        String message = event.getMessage().getText();
        try {
            String[] messageParts = message.split(" ", 2);
            String query = messageParts[0];
            String argument = messageParts[1];
            this.rootQueryHandler.handleQuery(query, argument, replyToken);
        } catch (QueryCannotBeHandledException | ArrayIndexOutOfBoundsException ex) {
            Message replyMessageContent = this.helpService.handle(this.rootQueryHandler);
            NoviReplyMessage replyMessage = new NoviReplyMessage(replyToken, replyMessageContent);
            this.sendMessage(replyMessage);
        } catch (Exception ex) {
            TextMessage replyMessageContent = new TextMessage("Maaf, service sedang mati. Mohon dicoba kembali nanti.");
            NoviReplyMessage replyMessage = new NoviReplyMessage(replyToken, replyMessageContent);
            this.sendMessage(replyMessage);
        }
    }

    @PostMapping("/send")
    public void sendMessage(@RequestBody NoviReplyMessage replyMessage) {
        String replyToken = replyMessage.getReplyToken();
        Message message = replyMessage.getMessage();

        ReplyMessage lineReplyMessage = new ReplyMessage(replyToken, message);
        lineClient.replyMessage(lineReplyMessage);
    }

    @PostConstruct
    private void constructQueryHandler() {
        this.lineClient = LineMessagingClient.builder(channelToken).build();
        this.rootQueryHandler = this.constructWatchAndBuyQueryHandler(null);
        this.rootQueryHandler = this.constructMovieServiceHandler(this.rootQueryHandler);
        this.rootQueryHandler = this.constructWatchlistQueryHandler(this.rootQueryHandler);
        this.rootQueryHandler = this.constructShowtimesServiceHandler(this.rootQueryHandler);
    }

    private QueryHandler constructWatchAndBuyQueryHandler(QueryHandler nextHandler) {
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/trailer",
                "Mencari trailer berdasarkan nama film. Contoh : /trailer avengers endgame"
        ));
        acceptedQueries.add(new AcceptedQuery(
                "/buy",
                "Mencari link untuk membeli film yang diinginkan di Google Play. Contoh : /buy zootopia"
        ));

        return new WatchAndBuyQueryHandler(acceptedQueries, nextHandler, watchAndBuyServiceClient);
    }

    private QueryHandler constructMovieServiceHandler(QueryHandler nextHandler) {
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/find",
                "Menunjukkan daftar film berdasarkan suatu keyword. Contoh : /find avengers"
        ));
        acceptedQueries.add(new AcceptedQuery(
                "/moviedetail",
                "Menampilkan sinopsis serta informasi mendetail mengenai suatu film. Contoh : /moviedetail zootopia"
        ));

        return new MovieServiceHandler(acceptedQueries, nextHandler, movieServiceClient);
    }

    private QueryHandler constructShowtimesServiceHandler(QueryHandler nextHandler) {
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/showtimes",
                "Menunjukkan daftar jam tayang suatu film di bioskop. Contoh : /showtimes interstellar"
        ));

        return new ShowtimesServiceHandler(acceptedQueries, nextHandler, showtimesServiceClient);
    }

    private QueryHandler constructWatchlistQueryHandler(QueryHandler nextHandler) {
        List<AcceptedQuery> acceptedQueries = new ArrayList<>();
        acceptedQueries.add(new AcceptedQuery(
                "/addfavorite",
                "Menambahkan film ke daftar favorite pribadi dengan argumen id movie pada tmbd. Contoh : /addfavorite 44214"
        ));

        return new WatchlistQueryHandler(acceptedQueries, nextHandler, watchlistServiceClient);
    }
}