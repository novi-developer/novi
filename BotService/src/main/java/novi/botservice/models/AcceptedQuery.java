package novi.botservice.models;

public class AcceptedQuery {
    private String keyword;
    private String description;

    public AcceptedQuery(String keyword, String description) {
        this.keyword = keyword;
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getDescription() {
        return description;
    }
}
