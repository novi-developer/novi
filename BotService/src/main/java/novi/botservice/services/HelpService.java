package novi.botservice.services;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import novi.botservice.handlers.QueryHandler;
import novi.botservice.models.AcceptedQuery;
import org.springframework.stereotype.Service;

@Service
public class HelpService {
    public Message handle(QueryHandler rootHandler) {
        StringBuilder helpText = new StringBuilder();
        helpText.append("Novi Bot Help Guide\n");
        helpText.append("Daftar Perintah : \n");

        QueryHandler handler = rootHandler;
        while (handler != null) {
            for (AcceptedQuery acceptedQuery : handler.getAcceptedQueries()) {
                helpText.append(acceptedQuery.getKeyword());
                helpText.append("\n");
                helpText.append(acceptedQuery.getDescription());
                helpText.append("\n\n");
            }
            handler = handler.getNextHandler();
        }

        TextMessage helpMessage = new TextMessage(helpText.toString());
        return helpMessage;
    }
}
