package novi.botservice.handlers;

import novi.botservice.clients.WatchAndBuyServiceClient;
import novi.botservice.exceptions.QueryCannotBeHandledException;
import novi.botservice.models.AcceptedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class WatchAndBuyQueryHandler extends QueryHandler {
    private static String BASE_URL = "https://novi-wbs.herokuapp.com";
    private static final Logger LOGGER = LoggerFactory.getLogger(WatchAndBuyQueryHandler.class);

    private WatchAndBuyServiceClient watchAndBuyServiceClient;

    public WatchAndBuyQueryHandler(List<AcceptedQuery> acceptedQueries, QueryHandler nextHandler,
                                   WatchAndBuyServiceClient watchAndBuyServiceClient) {
        super(acceptedQueries, nextHandler);
        this.watchAndBuyServiceClient = watchAndBuyServiceClient;
    }

    @Override
    @Async
    public void handleQuery(String query, String argument, String replyToken) throws QueryCannotBeHandledException {
        if (this.queryCanBeProcessed(query)) {
            try {
                this.watchAndBuyServiceClient.handleQuery(query, argument, replyToken);
            } catch (Exception ex) {
                LOGGER.warn("Exception occured on Watch and Buy Service Handler : " + ex.getMessage());
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> callBaseURL = restTemplate.getForEntity(BASE_URL, String.class);
                throw ex;
            }
        } else if (this.nextHandler != null) {
            this.nextHandler.handleQuery(query, argument, replyToken);
        } else {
            String errorMessage = "Query " + query + " tidak dapat diproses!";
            throw new QueryCannotBeHandledException(errorMessage);
        }
    }
}
