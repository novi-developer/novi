package novi.botservice.handlers;

import novi.botservice.exceptions.QueryCannotBeHandledException;
import novi.botservice.models.AcceptedQuery;

import java.util.List;

public abstract class QueryHandler {
    QueryHandler nextHandler;
    List<AcceptedQuery> acceptedQueries;

    QueryHandler(List<AcceptedQuery> acceptedQueries, QueryHandler nextHandler) {
        this.nextHandler = nextHandler;
        this.acceptedQueries = acceptedQueries;
    }

    public abstract void handleQuery(String query, String argument, String replyToken) throws QueryCannotBeHandledException;

    Boolean queryCanBeProcessed(String query) {
        for (AcceptedQuery acceptedQuery : acceptedQueries) {
            if (acceptedQuery.getKeyword().contains(query)) {
                return true;
            }
        }
        return false;
    }

    public List<AcceptedQuery> getAcceptedQueries() {
        return acceptedQueries;
    }

    public QueryHandler getNextHandler() {
        return nextHandler;
    }
}
