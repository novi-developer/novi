package novi.botservice.exceptions;

public class QueryCannotBeHandledException extends RuntimeException {
    public QueryCannotBeHandledException(String errorMessage) {
        super(errorMessage);
    }
}