# Novi
## Deskripsi
Sebuah chatbot yang memberikan kemudahan bagi penggunanya untuk mencari informasi terkini mengenai film.

## Status Pipeline and Coverage
- Master : [![pipeline status](https://gitlab.com/novi-developer/novi/badges/master/pipeline.svg)](https://gitlab.com/novi-developer/novi/commits/master) [![coverage report](https://gitlab.com/novi-developer/novi/badges/master/coverage.svg)](https://gitlab.com/novi-developer/novi/commits/master)
- Staging Movie Service : [![pipeline status](https://gitlab.com/novi-developer/novi/badges/staging-movieservice/pipeline.svg)](https://gitlab.com/novi-developer/novi/commits/staging-movieservice) [![coverage report](https://gitlab.com/novi-developer/novi/badges/staging-movieservice/coverage.svg)](https://gitlab.com/novi-developer/novi/commits/staging-movieservice)
- Staging Recommendation and Review Service : [![pipeline status](https://gitlab.com/novi-developer/novi/badges/staging-recommendationandreviewservice/pipeline.svg)](https://gitlab.com/novi-developer/novi/commits/staging-recommendationandreviewservice) [![coverage report](https://gitlab.com/novi-developer/novi/badges/staging-recommendationandreviewservice/coverage.svg)](https://gitlab.com/novi-developer/novi/commits/staging-recommendationandreviewservice)
- Staging Watch and Buy Service : [![pipeline status](https://gitlab.com/novi-developer/novi/badges/staging-watchandbuyservice/pipeline.svg)](https://gitlab.com/novi-developer/novi/commits/staging-watchandbuyservice) [![coverage report](https://gitlab.com/novi-developer/novi/badges/staging-watchandbuyservice/coverage.svg)](https://gitlab.com/novi-developer/novi/commits/staging-watchandbuyservice)
- Staging Showtimes Service : [![pipeline status](https://gitlab.com/novi-developer/novi/badges/staging-showtimesservice/pipeline.svg)](https://gitlab.com/novi-developer/novi/commits/staging-showtimesservice) [![coverage report](https://gitlab.com/novi-developer/novi/badges/staging-showtimesservice/coverage.svg)](https://gitlab.com/novi-developer/novi/commits/staging-showtimesservice)
- Staging Watchlist Service : [![pipeline status](https://gitlab.com/novi-developer/novi/badges/staging-watchlistservice/pipeline.svg)](https://gitlab.com/novi-developer/novi/commits/staging-watchlistservice) [![coverage report](https://gitlab.com/novi-developer/novi/badges/staging-watchlistservice/coverage.svg)](https://gitlab.com/novi-developer/novi/commits/staging-watchlistservice)

## Developer Guide
### Microservice Architecture

Arsitektur *Microservice* adalah arsitektur aplikasi dimana sebuah aplikasi (**Novi**) terbagi menjadi beberapa aplikasi kecil (*microservices*) yang independen, dimana *microservices* ini dapat saling berinteraksi guna memenuhi apa yang diinginkan oleh pengguna.

Umumnya satu *microservice* memenuhi satu *responsibility* saja, serta dapat berdiri sendiri, sehingga bila salah satu *microservice*, aplikasi tetap dapat melayani pengguna yang menggunakan fitur *microservice* lain.

**Novi** terbagi menjadi 7 buah *microservices*, yaitu :
- Eureka Registry Service (ERS)
  
  Karena dalam arsitektur *microservices* terdapat banyak *microservice*, tentu setiap *microservice* perlu mengetahui lokasi atau URL dari *microservice* lain bila ingin menggunakan fungsi atau fitur dari *microservice* tersebut. Eureka Registry Service berfungsi sebagai pusat penyimpanan lokasi tiap *microservice*, sehingga memudahkan *microservice* bila ingin mencari tahu lokasi *microservice* lain. Analogikan Eureka Registry Service dengan Buku Kuning Telepon dimana kita dapat dengan mudah mengetahui nomor telepon jasa yang kita ingin gunakan.  
  Tidak memiliki *branch* *staging* khusus.

- Bot Service (BS)
  
  *Microservice* yang mengatur segala hal yang berhubungan dengan *bot* **Novi**, seperti menerima dan mengirim pesan dari/ke pengguna serta menyimpan data profil pengguna.
  Tidak memiliki *branch* *staging* khusus.

- Movie Service (MS)
  
  *Branch* *staging*: **staging-movieservice**

- Recommendation and Review Service (RRS)
  
  *Branch* *staging*: **staging-recommendationandreviewservice**

- Watch and Buy Service (WBS)
  
  *Branch* *staging*: **staging-watchandbuyservice**

- Showtimes Service (STS)
  
  *Branch* *staging*: **staging-showtimesservice**

- Watchlist Service (WLS)
  
  *Branch* *staging*: **staging-watchlistservice**

### Developing your own *microservice*
#### Main Class
Semua *microservice* telah memiliki folder masing-masing. Untuk *skeleton code*, lihat di Watch And Buy Service.

Umumnya, *main class* dari *microservice* **Novi** akan berbentuk sebagai berikut.
```
package novi.watchandbuyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableAutoConfiguration    //Konfigurasi otomatis courtesy of Spring.
@EnableDiscoveryClient      //Agar microservice ini dapat menggunakan Eureka
@SpringBootApplication      //Menandakan bahwa ini adalah aplikasi Spring
public class WatchAndBuyServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(WatchAndBuyServiceApp.class, args);
    }
}

```
Pada *main class*, umumnya akan dideklarasikan Singleton yang digunakan oleh *microservice*, contohnya EurekaClient dan Repository (ORM ke Database)
```
    //Lanjutan kode diatas
    public static void main(String[] args) {
        SpringApplication.run(WatchAndBuyServiceApp.class, args);
    }

    @Bean   //Menandakan bahwa objek dibawah dapat diakses oleh semua class dalam package
    public EurekaClient eurekaClient = new EurekaClient();

```
Lalu, terdapat konfigurasi aplikasi, biasanya dideklarasikan di src/main/resources/application.yml. Biasanya akan berbentuk sebagai berikut.
```
spring:
  application:
    name: watchAndBuyService    //Nama dari aplikasi di daftar Eureka

eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8000/eureka/ //URL dari service Eureka

server:
  port: 8010    //Port yang akan digunakan aplikasi. Jangan sampai sama dengan service lain ya!
```
Cukup dengan kode tersebut saja, maka aplikasi Spring anda sudah bisa berjalan, wow!

**Recommended Read!**
- https://spring.io/blog/2015/07/14/microservices-with-spring

#### How to run?
Di folder masing-masing *microservice*, run this in terminal -> ```gradle bootRun```  
Seriously, gitu doang.

Untuk jalanin secara keseluruhan, urutan ```gradle bootRun``` adalah:
- EurekaRegistryService
- BotService
- Sisanya

Akan dibuat task gradle buat untuk melakukan run keseluruhan.

#### Controller and Service
Loh, itu kan baru kosongan saja, lalu dimana meletakkan method-method dari fitur saya? Tenang, sekarang kita akan membahas **Controller** dan **Service**.

Pertama, kita analogikan dengan hal yang lebih familiar, yaitu Django (saya asumsikan kalian dulu PPW-nya Django). Ingat **urls.py** dan **views.py**? **Controller** dapat dianalogikan sebagai **urls.py**, dan **Service** sebagai **views.py**. 

Karena kita hidup di dunia Java, sebenarnya **Controller** tidak wajib hanya mengatur alur dari URL yang dipanggil, bisa saja menjadi lebih *powerful* karena sebenarnya **Controller** bisa merangkap sebagai **Service**. Namun, ingat *Single Responsibility Principle*, sehingga kaidahnya adalah **Controller** melakukan pemetaan URL ke **Service** (Namun bisa juga melakukan sedikit *preprocessing*, misalnya memotong data yang dikirim ke URL), serta **Service**lah yang berisi Logic dari aplikasi, seperti memproses data dan menyimpannya ke Database, atau melakukan pengambilan data dan mengembalikannya dalam bentuk JSON.

A typical REST Controller (Controller yang mengembalikan JSON) looks like this.
```
@RestController //Menandakan bahwa kelas merupakan Controller
public class BotServiceController {
    //This roughly means method test() akan dipanggil saat user melakukan GET terhadap url /test, dan test() akan mengembalikan JSON
    @RequestMapping(value = "/test", method = method = RequestMethod.GET, produces = "application/json")
    public String test() {
        String message = "Hello World";
        return message;
    }
}
```

**Service** sekip dulu :)

**Recommended Read!**
- https://www.baeldung.com/spring-controllers
- https://www.baeldung.com/spring-controller-vs-restcontroller

#### How to nembak API atau Microservice sendiri
**Novi** kebanyakan akan melakukan pengambilan data dari Internet. Oleh karena itu, perlu cara yang mudah untuk melakukan tembak API. Fortunately for use, ada yang namanya **RestTemplate**.

##### What is RestTemplate?
Baca di https://www.baeldung.com/rest-template, or you know, Google it.

##### How to use?
First, use Singleton Pattern to the fullest. Don't waste memory and time by repeatedly making new instance of RestTemplate. Di main class *microservice** tambahkan ini.
```
    @Bean
    public RestTemplate restTemplate = new RestTemplate();
```
Lalu, berikut contoh memakai RestTemplate untuk GET dari suatu API.
```
    @Service
    public class MyAwesomeService {
        @Autowired  //Magic dari Spring yang ngambil instance yang dideclare di main class
        public RestTemplate restTemplate;

        @RequestMapping(value = "/test", method = method = RequestMethod.GET, produces = "application/json")
        public String myAwesomeMethod() {
            String API_URL = "http://cahya-ganteng.com/ngambilstring";
            String dapetDariCahyaGanteng = restTemplate.getForObject(API_URL, String.class);
            return dapetDariCahyaGanteng;
        }
    }
```
What just happen? Anda berikan restTemplate URL API sama jenis object yang bakal diterima, lalu restTemplate yang akan melakukan fetch untuk anda. That's it!

Notes: Menggunakan Singleton Pattern tidak mandatory, bisa pakai cara biasa (membuat restTemplate dalam method langsung atau dalam class langsung, tidak menggunakan Autowired). We can always refactor later :)
##### Wait, how about talking to our other microservice?
Story for another time :)

**Recommended Read!**
- https://spring.io/guides/gs/consuming-rest/
- https://www.baeldung.com/rest-template
- https://www.baeldung.com/spring-component-repository-service
